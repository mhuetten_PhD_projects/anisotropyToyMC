# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
## give default variables:
# grid resolution:
# NSIDE = 2**9
# # gaussian width of fov in degs:
# sigmaFOVdeg = 2.5
# # center of fov on celestial sphere:
# phi0deg =  0.0
# theta0deg = 0.0
# # ROI width:
# ROIwidthfactor = 10
# # gaussian width of point spread function:
# sigmaPSFdeg = 0.1
# # number of simulated events:
# nevents = 10**4
# # ratio signal events to isotropic background noise events:
# # fsig = N_sig/(N_sig + N_back)
# fsig = 0.0
# # power spectrum index for simulated skymaps:
# s = 2.0
# # ratio DM induced events to astrophysical events:
# fDM = 0.0
# # runnumber:
# runnumber = "testlocal5"
# output directory:
directory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"

# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:f:p:l:w:v:t:e:g:d:i:o:",["nside=","fov=","psf=","psi=","theta=","psiWidthDeg=","thetaWidthDeg=","events=","fsig=","fDM=","infile=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -l <psi0deg>        (no standard)'
    print ' -t <theta0deg>      (no standard)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -i <input file>     (no standard)'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -l <psi0deg>        (no standard)'
        print ' -t <theta0deg>      (no standard)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -i <input file>     (no standard)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-l", "--psi"):
        psi0deg = float(arg)
    elif opt in ("-t", "--theta"):
        theta0deg = float(arg)
    elif opt in ("-w", "--psiWidthDeg"):
        psiWidthDeg = float(arg)
    elif opt in ("-v", "--thetaWidthDeg"):
        thetaWidthDeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-i", "--infile"):
        inputfile = arg
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg

# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)


## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)

theta0degLon = 90.0 - theta0deg
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)*2.0/np.sqrt(np.pi) # compare with healpix documentation: this calculation gives the full diameter of a circular assumed region.
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*resolution
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars


# give out input variables:
print  " "
print  "** INPUT PARAMETERS: **"
print "NSIDE: ",NSIDE
print "Number of grid pixels on whole sphere: ",npix
print "Using gaussian FOV acceptance..."
print "Full diameter of FOV acceptance (1 sigma): ",sigmaFOVdeg,"°"
print "Full diameter of gaussian PSF (1 sigma): ",sigmaPSFdeg,"°" 
print "Center position in sky in celestial coord, latitude: ",theta0degLon,"°" 
print "Center position in sky in celestial coord, longitude ",psi0deg,"°" 
print "Resolution of skymap in degs: ",resolutiondeg, "°"
print "Number of MC events: ",nevents
print "Ratio fsig: ",fsig
print "Ratio fDM: ",fDM
print "Maximum resolvable multipole index l (lmax): ",lmax
print "** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS ADD FLAG -h AT STARTING **"
print  " "



print "plotting power spectrum..."


runnumber=[10000,100000,1000000,10000000]

print inputfile

#Load output spectrum:
filename = "clOutput"
inputname_clOutput = inputfile+"/nevents-"+str(runnumber[0])+"/%s.fits" % filename 


data = pf.getdata(inputname_clOutput)
clOUT = data.field(0)
firstspectrum = clOUT[1:]


clspectrum = np.ndarray((len(runnumber), len(firstspectrum)))
print clspectrum.shape

for i in range(len(runnumber)):
    print i
    inputname_clOutput = str(outputdirectory)+"/nevents-"+str(runnumber[i])+"/%s.fits" % filename 

    data = pf.getdata(inputname_clOutput)
    clOUT = data.field(0)
    
    inputspectrum = clOUT[1:]
    for j in range(len(inputspectrum)):
        clspectrum[i,j] = inputspectrum[j]

plot_powerspectrum = plt.figure(figsize=(12, 8)) 


     
ell = np.arange(len(firstspectrum))
print len(ell)
# 
# 
for i in range(len(runnumber)):
    if i==0:
        COLOR = 'k'
    if i==1:
        COLOR = 'r'
    if i==2:
        COLOR = 'magenta'
    if i==3:
        COLOR = 'darkviolet'
    if i==4:
        COLOR = 'b'
    if i==5:
        COLOR = 'darkturquoise'
    if i==6:
        COLOR = 'orange'
    if i==7:
        COLOR = 'yellow'
    if i==8:
        COLOR = 'violet'
    if i==9:
        COLOR = 'darkgray'
    if i==10:
        COLOR = 'pink'
        
    p1 = plt.plot(ell, ell * (ell+1) * clspectrum[i,:]/(2*np.pi),color=COLOR,  linestyle='-', linewidth=1, label="n = "+str(runnumber[i]))   
    

#             
title = 'Angular power spectrum from normalized event skymap for total events n,  sig. to noise ratio = %s, Gaussian FOV = %s degs., DM ratio = %s,  resolution = %s degs.'%(fsig,sigmaFOVdeg,fDM,round(resolutiondeg,3))
plt.title('\n'.join(wrap(title,80)))
plt.subplots_adjust(top=0.85)
plt.legend(loc='upper left')
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
plt.xscale('log')
plt.yscale('log')
pylab.xlim([1,lmax])
pylab.ylim([10**(-4),10**6])
#      
plt.show()

print "finished."
print ""
