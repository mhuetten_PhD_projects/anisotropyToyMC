#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
   echo
   echo "Evaluation script for  Clumpy MonteCarlo Simulation:"
   echo
   echo "fullSimulationSTATISTICAL.spectraplots.sh <full_path_outputdirectory> " 
   echo


   exit
fi

#read variables from parameter file:

outputdirpath=$1



# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"


dmClumpsparameterfile=${outputdirpath}/Inputfiles/dmClumps.parameters
clumpyMCparameterfile=${outputdirpath}/Inputfiles/clumpyMC.parameters

#read variables from dmClumps.parameters file:

# General parameters:
#export runname=$(cat $dmClumpsparameterfile 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export outdir=$(cat $dmClumpsparameterfile 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $dmClumpsparameterfile 		| head -n15 | tail -n1 | sed -e 's/^[ \t]*//') 
export alphaIntDeg=$(cat $dmClumpsparameterfile 	| head -n18 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $dmClumpsparameterfile 		| head -n24 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $dmClumpsparameterfile 	| head -n27 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $dmClumpsparameterfile 	| head -n30 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $dmClumpsparameterfile 	| head -n33 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $dmClumpsparameterfile | head -n39 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $dmClumpsparameterfile 	| head -n45 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $dmClumpsparameterfile | head -n48 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $dmClumpsparameterfile | head -n49 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $dmClumpsparameterfile | head -n50 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $dmClumpsparameterfile 		| head -n55 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $dmClumpsparameterfile 			| head -n58 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $dmClumpsparameterfile 				| head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $dmClumpsparameterfile 				| head -n64 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $dmClumpsparameterfile 	 | head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $dmClumpsparameterfile | head -n73 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $dmClumpsparameterfile | head -n74 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $dmClumpsparameterfile | head -n75 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $dmClumpsparameterfile		 | head -n78 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDM_SLOPE=$(cat $dmClumpsparameterfile			 | head -n81 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $dmClumpsparameterfile			 | head -n84 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $dmClumpsparameterfile		 | head -n87 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $dmClumpsparameterfile   | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $dmClumpsparameterfile | head -n96 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $dmClumpsparameterfile | head -n97 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $dmClumpsparameterfile | head -n98 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $dmClumpsparameterfile  | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')

# Additional parameters:
export gDM_MMAXFRAC_SUBS=$(cat $dmClumpsparameterfile | head -n107 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_RHOSAT=$(cat $dmClumpsparameterfile  | head -n110 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M1=$(cat $dmClumpsparameterfile | head -n113 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M2=$(cat $dmClumpsparameterfile  | head -n116 | tail -n1 | sed -e 's/^[ \t]*//')



#read variables from clumpyMC.parameters file:

# General parameters:

export NSIDE=$(cat $clumpyMCparameterfile 		| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated telescope properties:
export sigmaFOVdeg=$(cat $clumpyMCparameterfile | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')
#psi0deg=$psiZeroDeg
#theta0deg=$(( 90 - $thetaZeroDeg )) 
export sigmaPSFdeg=$(cat $clumpyMCparameterfile | head -n35 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated physics:
export nevents=$(cat $clumpyMCparameterfile 	| head -n41 | tail -n1 | sed -e 's/^[ \t]*//')
export fsig=$(cat $clumpyMCparameterfile 		| head -n44 | tail -n1 | sed -e 's/^[ \t]*//')
export fDM=$(cat $clumpyMCparameterfile 		| head -n47 | tail -n1 | sed -e 's/^[ \t]*//')




# If a run parameter file exists:
if [ -e "${outputdirpath}/Inputfiles/fullSimulationSTATISTICAL.runparameter" ]
then
	echo " evaluate multi run simulation"
	PLIST=${outputdirpath}/Inputfiles/fullSimulationSTATISTICAL.runparameter
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n " Running parameter is: "
	echo $runparameter
	
	for AFIL in  $PARAMETERS
	do

	echo " now evaluating parameter run $runparameter = $AFIL"

   case $runparameter in
	"user_rse") user_rse=$AFIL;;
	"alphaIntDeg") alphaIntDeg=$AFIL;;
	"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$AFIL;;
	"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$AFIL;;
	"gGAL_RHOSOL") gGAL_RHOSOL=$AFIL;;
	"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$AFIL;;
	"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$AFIL;;
	"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$AFIL;;
	"nevents") nevents=$AFIL;;
	"fsig") fsig=$AFIL;;
	"fDM") fDM=$AFIL;;
   esac

	# rename $runname into:	
	subrunpath=${outputdirpath}/$runparameter-$AFIL

	# grep numbe rof runs:
	cd $subrunpath/spectra-clumpy
	realisations=$(ls -1 | wc -l)

python $SCRIPTDIR/fullSimulationSTATISTICAL.spectraplots.py -n $NSIDE -t $thetaZeroDeg -f $sigmaFOVdeg -p $sigmaPSFdeg -d $fDM -i $subrunpath -l $psiZeroDeg -e $nevents -g $fsig -r $realisations -v $psiWidthDeg -w $thetaWidthDeg -a $alphaIntDeg -u $user_rse -m $gDM_MMIN_SUBS &
	done
	realisations=100
	#python $SCRIPTDIR/fullSimulationSTATISTICAL.combineSpectraplots.py -n $NSIDE -t $thetaZeroDeg -f $sigmaFOVdeg -p $sigmaPSFdeg -d $fDM -i ${outputdirpath} -l $psiZeroDeg -e $nevents -g $fsig -r $realisations -v $psiWidthDeg -w $thetaWidthDeg -a $alphaIntDeg -u $user_rse -m $gDM_MMIN_SUBS &
 
fi

outdir=$outputdirpath
echo $outdir


if [ ! -e "${outputdirpath}/Inputfiles/fullSimulationSTATISTICAL.runparameter" ]
then

echo " evaluating single run"

# grep number of runs:
cd $outputdirpath/spectra-mc
realisations=$(ls -1 | wc -l)
cd $SCRIPTDIR

python $SCRIPTDIR/fullSimulationSTATISTICAL.spectraplots.py -n $NSIDE -l $psiZeroDeg -t $thetaZeroDeg -v $psiWidthDeg -w $thetaWidthDeg -f $sigmaFOVdeg -i $outdir -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM  -o $outdir -r $realisations -u $user_rse -m $gDM_MMIN_SUBS -a $alphaIntDeg & #>> Plot.log


fi




exit

