#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
   echo
   echo "Evaluation script for  Clumpy MonteCarlo Simulation:"
   echo
   echo "clumpyMC.plots.sh <outputdirectory> " 
   echo
   echo
   echo "  parameter should contain parameter numbers only"
   echo "  example for parameter list:"
   echo "    0.0"
   echo "    0.2"
   echo "    0.5"
   echo

   exit
fi

#read variables from parameter file:

runname=$1





# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"


WORKDIR=/lustre/fs5/group/cta/users/mhuetten/workdata/anisotropyMC

parameterfile=${WORKDIR}/${runname}/Inputfiles/clumpyMC.parameters

#read variables from parameter file:

# General parameters:
#export runname=$(cat $parameterfile 	| head -n10 | tail -n1 | sed -e 's/^[ \t]*//')
#export outdir=$(cat $parameterfile 		| head -n13 | tail -n1 | sed -e 's/^[ \t]*//')
export inputfile=$(cat $parameterfile 	| head -n16 | tail -n1 | sed -e 's/^[ \t]*//')
export NSIDE=$(cat $parameterfile 		| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated telescope properties:
export sigmaFOVdeg=$(cat $parameterfile | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')
export psi0deg=$(cat $parameterfile 	| head -n28 | tail -n1 | sed -e 's/^[ \t]*//')
export theta0deg=$(cat $parameterfile 	| head -n32 | tail -n1 | sed -e 's/^[ \t]*//')
export sigmaPSFdeg=$(cat $parameterfile | head -n35 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated physics:
export nevents=$(cat $parameterfile 	| head -n41 | tail -n1 | sed -e 's/^[ \t]*//')
export fsig=$(cat $parameterfile 		| head -n44 | tail -n1 | sed -e 's/^[ \t]*//')
export fDM=$(cat $parameterfile 		| head -n47 | tail -n1 | sed -e 's/^[ \t]*//')

# If a run parameter file exists:

outdir=${WORKDIR}/${runname}/
echo $outdir

		
python $SCRIPTDIR/clumpyMC.merge_neventsSpectra-14-02-11.py -n $NSIDE -l $psi0deg -t $theta0deg -v 10.0 -w 10.0 -f $sigmaFOVdeg -i $outdir -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM  -o $outdir #>> Plot.log







exit

