# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from matplotlib.patches import Rectangle
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
## give default variables:
# grid resolution:
# NSIDE = 2**9
# # gaussian width of fov in degs:
# sigmaFOVdeg = 2.5
# # center of fov on celestial sphere:
# phi0deg =  0.0
# theta0deg = 0.0
# # ROI width:
# ROIwidthfactor = 10
# # gaussian width of point spread function:
# sigmaPSFdeg = 0.1
# # number of simulated events:
# nevents = 10**4
# # ratio signal events to isotropic background noise events:
# # fsig = N_sig/(N_sig + N_back)
# fsig = 0.0
# # power spectrum index for simulated skymaps:
# s = 2.0
# # ratio DM induced events to astrophysical events:
# fDM = 0.0
# # runnumber:
# runnumber = "testlocal5"
# output directory:
directory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"

# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:f:p:l:w:v:t:e:g:d:i:o:r:u:m:a:",["nside=","fov=","psf=","psi=","theta=","psiWidthDeg=","thetaWidthDeg=","events=","fsig=","fDM=","indir=","dirout=","realisations=","user_rse=","minmass=","alpha_int="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -l <psi0deg>        (no standard)'
    print ' -t <theta0deg>      (no standard)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -i <input directory>     (no standard)'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -l <psi0deg>        (no standard)'
        print ' -t <theta0deg>      (no standard)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -i <input directory>     (no standard)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-l", "--psi"):
        psi0deg = float(arg)
    elif opt in ("-t", "--theta"):
        theta0deg = float(arg)
    elif opt in ("-w", "--psiWidthDeg"):
        psiWidthDeg = float(arg)
    elif opt in ("-v", "--thetaWidthDeg"):
        thetaWidthDeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-i", "--indir"):
        inputdirectory = arg
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg
    elif opt in ("-r", "--realisations"):
        realisations = int(arg)
    elif opt in ("-u", "--user_rse"):
        user_rse = float(arg)
    elif opt in ("-m", "--minmass"):
        minmass = float(arg)
    elif opt in ("-a", "--alpha_int"):
        alpha_int = float(arg)
# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)


## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)

theta0degLon = theta0deg
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)*2.0/np.sqrt(np.pi) # compare with healpix documentation: this calculation gives the full diameter of a circular assumed region.
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*resolution
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars


# give out input variables:
print  " "
print  "** INPUT PARAMETERS: **"
print "Input directory: ", inputdirectory
print "NSIDE: ",NSIDE
print "Number of grid pixels on whole sphere: ",npix
print "Using gaussian FOV acceptance..."
print "Full diameter of FOV acceptance (1 sigma): ",sigmaFOVdeg,"°"
print "Full diameter of gaussian PSF (1 sigma): ",sigmaPSFdeg,"°" 
print "Center position in sky in celestial coord, latitude: ",theta0degLon,"°" 
print "Center position in sky in celestial coord, longitude ",psi0deg,"°" 
print "Resolution of skymap in degs: ",resolutiondeg, "°"
print "Number of MC events: ",nevents
print "Ratio fsig: ",fsig
print "Ratio fDM: ",fDM
print "Maximum resolvable multipole index l (lmax): ",lmax
print "Number of evaluated sample realisations: ",realisations
print "** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS ADD FLAG -h AT STARTING **"
print  " "




runparameter=[1,0.5,0.1,0.05,0.01,0.005,0.002,0.001,0.0]

totalruns=np.arange(realisations*len(runparameter))
runnumbers=np.arange(realisations)

print "number of evaluated sample realisations (check output): ", len(totalruns)

################################# clumpy data#######################################

#Load first dmClumps simulation output spectrum:
firstspectrumfilepath = inputdirectory+"/fsig-"+str(runparameter[0])+"/spectra-clumpy/clumpy-realisation-1.spectrum.fits"
 
 
data = pf.getdata(firstspectrumfilepath)
clOUT = data.field(0)
firstspectrum = clOUT[1:]
 
 
clumpyspectrum = np.ndarray((len(totalruns), len(firstspectrum)))
print "dimension of combined clumpy spectra data matrix: ", clumpyspectrum.shape
 
print "plotting clumpy power spectra..."
 
for i in range(len(runparameter)): 
    
    for j in range(len(runnumbers)):
        print i,j
        clumpyspectrafilepath = inputdirectory+"/fsig-"+str(runparameter[i])+"/spectra-clumpy/clumpy-realisation-"+str(runnumbers[j]+1)+".spectrum.fits"
  
        data = pf.getdata(clumpyspectrafilepath)
        clOUT = data.field(0)
      
        inputspectrum = clOUT[1:]
        for k in range(len(inputspectrum)):
            clumpyspectrum[i*realisations+j,k] = inputspectrum[k]
  
plot_powerspectrum = plt.figure(figsize=(12, 8)) 
  
  
       
ell = np.arange(len(firstspectrum))
print len(ell)
# 
# 
for i in range(len(totalruns)):
 
    COLOR = 'pink'
          
    p1 = plt.plot(ell, ell * (ell+1) * clumpyspectrum[i,:]/(2*np.pi),color=COLOR,  linestyle='-', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1))   
  
  
# # include background bands:
# backgrundruns = 2
# backgrundclspectrum = np.ndarray((backgrundruns, len(firstspectrum)))
#   
# for i in range(backgrundruns):
#     print i
#     inputname_clOutput = str(outputdirectory)+"/Background"+str(i+1)+"/%s.fits" % filename 
#   
#     data = pf.getdata(inputname_clOutput)
#     clOUT = data.field(0)
#       
#     inputspectrum = clOUT[1:]
#     for j in range(len(inputspectrum)):
#         backgrundclspectrum[i,j] = inputspectrum[j]    
#   
#   
# backgrundclbands = np.ndarray((2, len(firstspectrum)))
#   
# for k in range( len(firstspectrum)):
#         backgrundclbands[0,k]= min(backgrundclspectrum[:,k])
#         backgrundclbands[1,k]= max(backgrundclspectrum[:,k])
#   
# p1 = plt.fill_between(ell, ell * (ell+1) * backgrundclbands[0,:]/(2*np.pi),ell * (ell+1) * backgrundclbands[1,:]/(2*np.pi),color='gray',alpha=1., label="Isotropic background")
#                
title = 'Angular power spectrum of CLUMPY skymap  around galactic position (Psi,theta)=(%s,%s) degrees,  width of simulated map (Delta Psi,Delta theta)=(%s,%s). Resolution = %s degs, user_rse = %s minimal clump mass = %s M_sun and %s realisations.'%(psi0deg,theta0deg,psiWidthDeg,thetaWidthDeg,alpha_int,user_rse,minmass,realisations*len(runparameter))
plt.title('\n'.join(wrap(title,80)))
plt.subplots_adjust(top=0.85)
plt.legend(loc='upper left')
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
plt.xscale('log')
plt.yscale('log')
pylab.xlim([1,lmax])
#pylab.ylim([10**(-4),10**6])
#plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
 
      
#plt.show()

######### histogram: ###########

ellhist=500

#hist, bins = np.histogram(ellhist * (ellhist+1) *clumpyspectrum[:,ellhist]/(2*np.pi), bins=20)

#width = 0.7 * (bins[1] - bins[0])
#center = (bins[:-1] + bins[1:]) / 2
plot_histogram = plt.figure(figsize=(12, 8)) 

#plt.bar(center, hist, align='center')#, width=width)
plt.hist(ellhist * (ellhist+1) *clumpyspectrum[:,ellhist]/(2*np.pi),bins=np.logspace(28,32,num=50),range=[10**(28),10**32], align='mid')#=50,range=[10**29,10**30])#, 10,  facecolor='blue', alpha=0.75)#
plt.gca().set_xscale("log")#
#ax.set_xlim(10**(28),10**32)
title = 'Histogram @ multipole moment l=%s for angular power spectrum of CLUMPY skymap  around galactic position (Psi,theta)=(%s,%s) degrees,  width of simulated map (Delta Psi,Delta theta)=(%s,%s). Resolution = %s degs, user_rse = %s minimal clump mass = %s M_sun and %s realisations.'%(ellhist,psi0deg,theta0deg,psiWidthDeg,thetaWidthDeg,alpha_int,user_rse,minmass,realisations*len(runparameter))
plt.title('\n'.join(wrap(title,80)))
plt.xlabel('l(l+1)c_l/2pi'); plt.ylabel('# realisations in bin')
#################################



plt.show()

print "finished."
print ""
