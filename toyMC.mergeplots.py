# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
## give default variables:
# grid resolution:
NSIDE = 2**12
# gaussian width of fov in degs:
sigmaFOVdeg = 5.0
# center of fov on celestial sphere:
phi0deg =  0.0
theta0deg = 0.0
# ROI width:
ROIwidthfactor = 10
# gaussian width of point spread function:
sigmaPSFdeg = 0.1
# number of simulated events:
nevents = 10**7
# ratio signal events to isotropic background noise events:
# fsig = N_sig/(N_sig + N_back)
fsig = 1.0
# power spectrum index for simulated skymaps:
s = 2.0
# ratio DM induced events to astrophysical events:
fDM = 0.0
# snumber:
snumber = "testlocal5"
# output directory:
directory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"

# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:r:f:p:e:g:d:s:o:",["nside=","roi=","fov=","psf=","events=","fsig=","fDM=","s=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -r <ROIwidthfactor> (Standard: 10)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -s <index s>,       (Standard: 0.0)'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -r <ROIwidthfactor> (Standard: 10)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -s <index s>,       (Standard: 0.0)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-r", "--roi"):
        ROIwidthfactor = float(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-s", "--s"):
        s = float(arg) 
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg

# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)


## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)
# give celestial position in rads:
phi0 =  np.pi*phi0deg/180.0
phi0lat = -phi0 # healpy counts phi clockwise!!
theta0 = np.pi*theta0deg/180.0
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars


# give out input variables:
print  " "
print  "** INPUT PARAMETERS: **"
print "NSIDE: ",NSIDE
print "Number of grid pixels on whole sphere: ",npix
print "Using gaussian FOV acceptance..."
print "Full diameter of FOV acceptance (1 sigma): ",sigmaFOVdeg,"°"
print "Full diameter of gaussian PSF (1 sigma): ",sigmaPSFdeg,"°" 
print "Resolution of skymap in degs: ",resolutiondeg, "°"
print "Full diameter of ROI: ",ROIwidthfactor, "*",sigmaFOVdeg,"°"
print "Number of MC events: ",nevents
print "Ratio fsig: ",fsig
print "Ratio fDM: ",fDM
print "Power spectrum index s: ",s
print "Maximum resolvable multipole index l (lmax): ",lmax
print "** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS ADD FLAG -h AT STARTING **"
print  " "








runnumber=[1,2,3,4,5,6]
snumber=[0.0,2.5,2.0,1.5,1.0,0.5]

clOUTspectra = np.zeros(shape=(lmax-3,len(runnumber),len(snumber)))
clOUTbands = np.zeros(shape=(lmax-3,len(snumber)*2))
clSIM = np.zeros(shape=(lmax-3,len(snumber))) # simulated "pure" power spectrum without noise  
clnormFIT = [clnorm*0.127,clnorm*0.08,clnorm*3,clnorm*100,clnorm*3500,clnorm*120000]



clspectrum = np.ndarray((len(snumber), lmax+1))

plot_powerspectrum = plt.figure(figsize=(9, 6)) 
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()  
plt.xscale('log')
plt.yscale('log')
print "Load power spectrum files..."


for i in range(len(snumber)):
    if snumber[i] == 0.0:
        exp = 2.0
    else:
        exp = snumber[i]
    for l in range(1,lmax-2):
        clSIM[l-1][i] =  clnormFIT[i]*l**exp/(l*(l+1))
            
    for j in range(len(runnumber)):
        

        
        if snumber[i]  == 0.0:
            outputdirectory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/events10e7Res4096Background"+str(runnumber[j])
            
            filename = str(snumber[i])+"_clOutput"
            inputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 
            clspectrum = pf.open(inputname_clOutput)
            data = pf.getdata(inputname_clOutput)
            
            clOUT = data.field(0)

            clOUT = clOUT[1:lmax-2]
            
            for k in range(len(clOUT)):
                clOUTspectra[k][j][i] = clOUT[k]
            
           
        else:
            outputdirectory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/events10e7Res4096FullAstroSignal"+str(runnumber[j])
    
            filename = str(snumber[i])+"_clOutput"
            inputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 
        
            clspectrum = pf.open(inputname_clOutput)
        
#             info = clspectrum.info()
#             print info
#             print ""
#             header = pf.getheader(inputname_clOutput)
#             print header
#             print header.keys()
#             
            data = pf.getdata(inputname_clOutput)
 
        
            clOUT = data.field(0)
            clOUT = clOUT[1:lmax-2]
           # print len(clOUT)
            for k in range(len(clOUT)):
                clOUTspectra[k][j][i] = clOUT[k]
                        
            #print len(ell)
            
            #ell = np.arange(len(clOUT))
            #print len(ell)
        #plt.plot(ell, (ell) * (ell+1) * clsim/(2*np.pi),color='b')# 
            #plt.plot(ell, ell * (ell+1) * clOUT/(2*np.pi))#
        #plt.plot(ell, ell * (ell+1) * clSIM/(2*np.pi),color='k')#
        #plt.plot(ell, ell * (ell+1) * clSIMnoise/(2*np.pi),'k--')#
        print i
	for k in range(lmax-3):
		#blubb =  max(clOUTspectra[k][:][i])
		clOUTbands[k][2*i]= max(clOUTspectra[k,:,i])
                clOUTbands[k][2*i+1]= min(clOUTspectra[k,:,i])
	
ell = np.arange(lmax-3)
print len(ell)


for i in range(len(snumber)):
    if i==0:
        COLOR = 'k'
    if i==1:
        COLOR = 'r'
    if i==2:
        COLOR = 'magenta'
    if i==3:
        COLOR = 'darkviolet'
    if i==4:
        COLOR = 'b'
    if i==5:
        COLOR = 'darkturquoise'
    p1 = plt.plot(ell, ell * (ell+1) * clSIM[:,i]/(2*np.pi),color='k',  linestyle=':', linewidth=1)   
    p2 = plt.fill_between(ell, ell * (ell+1) * clOUTbands[:,2*i]/(2*np.pi),ell * (ell+1) * clOUTbands[:,2*i+1]/(2*np.pi),color=COLOR,alpha=1)
    plt.legend([p1], ["Test1"], loc=1)
   # plt.fill_between(ell, ell * (ell+1) * clOUTspectra[:,0,i+1]/(2*np.pi),ell * (ell+1) * clOUTspectra[:,2,i+1]/(2*np.pi),color=COLOR,alpha=1)
   #ll * (ell+1) * clOUTspectra[:,1,i+1]/(2*np.pi),ell * (ell+1) * clOUTspectra[:,2,i+1]/(2*np.pi),color=COLOR,alpha=1)  
    
#plot background:
#plt.fill_between(ell, ell * (ell+1) * clOUTspectra[:,0,0]/(2*np.pi),ell * (ell+1) * clOUTspectra[:,1,0]/(2*np.pi),color='k',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * clOUTspectra[:,0,0]/(2*np.pi),ell * (ell+1) * clOUTspectra[:,2,0]/(2*np.pi),color='k',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * clOUTspectra[:,1,0]/(2*np.pi),ell * (ell+1) * clOUTspectra[:,2,0]/(2*np.pi),color='k',alpha=1)     

pylab.xlim([1,10**4])
pylab.ylim([10**(-4),10**6])
            
title = 'Angular power spectrum from normalized event skymap for total events n = %s,  sig. to noise ratio = %s, Gaussian FOV = %s$^\circ$, DM ratio = %s,  resolution = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,fDM,round(resolutiondeg,3))
plt.title('\n'.join(wrap(title,80)))
plt.subplots_adjust(top=0.85)




  
#### compare power spectra:
 
# compute power spectrum:
 
#clSIMnoise = np.zeros(lmax+1) # simulated power spectrum of noise
#LMAX = lmax
 
 
## re-compute input power spectrum with same normalization Integral_wholesphere = 4Pi\
## for comparison with output power spectrum:
# re-generate cl's:
#for l in range(1,lmax+2):
#    clSIM[l-1] =  1.0*l**s/(l*(l+1)) # the norm clnorm is not considered here because the spectrum
#                                     # is renormalized anyway.
    #clSIMnoise[l-1] =  10**(-5)*l**2/(l*(l+1)) # the norm clnorm is not considered here because the spectrum
#                                     # is renormalized anyway.

 
# save spectrum to file:
# pickle.dump(clOUT, outputfile_clspectrum)
# outputfile_clspectrum.close()
 
 

plt.show()

print "finished."
print ""

