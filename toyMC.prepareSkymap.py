#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np

# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import os
import sys
import getopt
#from matplotlib.backends.backend_pdf import PdfPages as pdf
#from textwrap import wrap
import pyfits as pf


# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:r:f:p:e:g:d:s:o:",["nside=","roi=","fov=","psf=","events=","fsig=","fDM=","s=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -r <ROIwidthfactor> (Standard: 10)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -s <index s>,       (Standard: 0.0)'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -r <ROIwidthfactor> (Standard: 10)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -s <index s>,       (Standard: 0.0)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-r", "--roi"):
        ROIwidthfactor = float(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-s", "--s"):
        s = float(arg) 
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg

# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)
phi0deg = 0.0
theta0deg = 0.0

## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)
print " Total number of pixels on whole skymap: ", npix
# give celestial position in rads:
phi0 =  np.pi*phi0deg/180.0
phi0lat = -phi0 # healpy counts phi clockwise!!
theta0 = np.pi*theta0deg/180.0
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# ROI width:
ROIwidth = ROIwidthfactor*2*sigmaFOV
ROIwidthdeg = np.pi*ROIwidth/180
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)
print " Resolution of skymap: ", resolutiondeg, "degrees"
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars





print " Write files to directory", outputdirectory


# Create Input Spectrum Filename:
filename = str(s)+"_clInput"
outputname_clInput = str(outputdirectory)+"/%s.fits" % filename 

# Create Skymap Filename:
filename = str(s)+"_skymap"
outputname_skymap = str(outputdirectory)+"/%s.fits" % filename 

# Create Synfast Input parameter file:
filename = str(s)+"_synfast"
outputname_synfastpar = str(outputdirectory)+"/%s.par" % filename 
outputfile_synfastpar = open(outputname_synfastpar, 'wb')


## generate Input spectrum:
print " Generate Input spectrum and write to fits file..."
clIN = np.zeros(lmax-1) # vector of c(l)'s
# generate cl's:
for l in range(1,len(clIN)):
    clIN[l] = clnorm*l**s/(l*(l+1))

col1 = pf.Column(name='C_l', format='D', array=clIN)
cols = pf.ColDefs([col1])
tbhdu = pf.new_table(cols)
prihdr = pf.Header()
#prihdr['COMMENT'] = "Here's some commentary about this FITS file."
prihdu = pf.PrimaryHDU(header=prihdr)
thdulist = pf.HDUList([prihdu, tbhdu])
thdulist.writeto(outputname_clInput)

sigmaPSFarcmin = 0.5*sigmaPSFdeg*60.0

print " Write Synfast parameter file synfast.par..."
# Fill Synfast parameter file synfast.par:
outputfile_synfastpar.write("simul_type = 1\n\n")
outputfile_synfastpar.write("nsmax = ")
outputfile_synfastpar.write(str(NSIDE))
outputfile_synfastpar.write("\n\n")
outputfile_synfastpar.write("nlmax = ")
outputfile_synfastpar.write(str(lmax))
outputfile_synfastpar.write("\n\n")
outputfile_synfastpar.write("infile = ")
outputfile_synfastpar.write(outputname_clInput)
outputfile_synfastpar.write("\n\n") 
outputfile_synfastpar.write("iseed = -1\n\n")
outputfile_synfastpar.write("fwhm_arcmin = ")
outputfile_synfastpar.write(str(sigmaPSFarcmin))
outputfile_synfastpar.write("\n\n")
outputfile_synfastpar.write("beam_file = ''\n\n")
outputfile_synfastpar.write("almsfile = ''\n\n")
outputfile_synfastpar.write("plmfile = ''\n\n")
outputfile_synfastpar.write("outfile = ")
outputfile_synfastpar.write(outputname_skymap)
outputfile_synfastpar.write("\n\n")
outputfile_synfastpar.write("outfile_alms = ''\n\n") #outfile_alms=$ODIR/${SPARAMETER}_alm.fits 
outputfile_synfastpar.write("apply_windows = f")
outputfile_synfastpar.close()

del clIN

print " Script toyMC.prepareSkymap.py successfully finished."
# end of script
