#!/bin/sh
#
# script run toy MonteCarlo simulations for DM-Anisotropies from clumpy maps
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Submission script for MonteCarlo Simulation based on clumpy skymaps:"
	echo
	echo "clumpyMC.sub.sh <clumpyMC.parameters>"
	echo
	echo "or"
	echo
	echo "clumpyMC.sub.sh <clumpyMC.parameters> <clumpyMC.runparameter>" 
	echo
	echo "  for the dmClumps.parameters file use template without changing line numbers"
	echo "  in case of choosing a varying parameter in a dmClumps.runparameter file,"
	echo "  write name of parameter in first line, then al values you want to compute."
	echo "  Example:"
	echo
	echo "    user_rse"
	echo "    5.0"
	echo "    10.0"
	echo "    15.0"
	echo
	
	exit
fi



#read variables from parameter file:

# General parameters:
export runname=$(cat $1 	| head -n10 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 		| head -n13 | tail -n1 | sed -e 's/^[ \t]*//')
export inputfile=$(cat $1 	| head -n16 | tail -n1 | sed -e 's/^[ \t]*//')
export NSIDE=$(cat $1 		| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated telescope properties:
export sigmaFOVdeg=$(cat $1 | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')
export psi0deg=$(cat $1 	| head -n28 | tail -n1 | sed -e 's/^[ \t]*//')
export theta0deg=$(cat $1 	| head -n32 | tail -n1 | sed -e 's/^[ \t]*//')
export sigmaPSFdeg=$(cat $1 | head -n35 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated physics:
export nevents=$(cat $1 	| head -n41 | tail -n1 | sed -e 's/^[ \t]*//')
export fsig=$(cat $1 		| head -n44 | tail -n1 | sed -e 's/^[ \t]*//')
export fDM=$(cat $1 		| head -n47 | tail -n1 | sed -e 's/^[ \t]*//')


if [ -n "$2" ]
then
	PLIST=$2
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"
# now forget outdir

# skeleton script
FSCRIPT="clumpyMC.qsub"

# If a parameter file exists:
if [ -n "$2" ]
then
	cd $RUNDIR
	rm -r Inputfiles
	mkdir -p Inputfiles
	cd $SCRIPTDIR
	cp clumpMC.* $RUNDIR/Inputfiles/

# now loop over all files in files loop:

for AFIL in $PARAMETERS
	do
	   echo "now starting parameter run $runparameter = $AFIL"
	
	
# create output directories:
		
		cd $RUNDIR
		mkdir $runparameter-$AFIL
		cd $runparameter-$AFIL
		ODIR="$(pwd)"
		
		rm $ODIR/skymap.fits
		rm $ODIR/clInput.fits
		rm $ODIR/clOutput.fits
		rm $ODIR/*logfile*
		rm -r Logfiles
		
		mkdir -p Logfiles
		mkdir -p Inputfiles
		cd $SCRIPTDIR
		cp clumpyMC.* $RUNDIR/Inputfiles/
		QLOG=$ODIR/Logfiles
		FNAM="$QLOG/clumpyMC-$runname-$runparameter-$AFIL"
			
	# output directory for error/output from batch system
	# in case you submit a lot of scripts: QLOG=/dev/null
			
			
			case $runparameter in
				"nevents") nevents=$AFIL;;
				"fsig") fsig=$AFIL;;
				"fDM") fDM=$AFIL;;
			esac
			
			sed	-e "s|PEEED|$QLOG|" \
				-e "s|OODIR|$ODIR|" \
				-e "s|DDDDD|$DATE|" \
				-e "s|NSSSE|$NSIDE|" \
				-e "s|SSSSS|$sigmaFOVdeg|" \
				-e "s|PPPPP|$psi0deg|" \
				-e "s|TTTTT|$theta0deg|" \
				-e "s|ROOOI|$inputfile|" \
				-e "s|PSSSF|$sigmaPSFdeg|" \
				-e "s|NNNNN|$nevents|" \
				-e "s|FFFFF|$fDM|" \
				-e "s|SIIIG|$fsig|" \
				-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh
			
			chmod u+x $FNAM.sh
			echo $FNAM.sh
			
		qsub -V -m bea -l h_cpu=47:59:00 -l h_rt=47:59:00  -l os=sl6 -l h_vmem=8G -l tmpdir_size=8G -o $QLOG/ -e $QLOG/ "$FNAM.sh"
		
	done
fi


if [ ! -n "$2" ]
then
	echo "now submitting single job to batch"
	
		
	cd $RUNDIR
	ODIR="$(pwd)"
		
	rm $ODIR/skymap.fits
	rm $ODIR/clInput.fits
	rm $ODIR/clOutput.fits
	rm $ODIR/*logfile*
		
	rm -r Logfiles
	rm -r Inputfiles
		
	mkdir -p Inputfiles
	mkdir -p Logfiles
	QLOG=$ODIR/Logfiles
	FNAM="$QLOG/clumpyMC-$runname"
		
	cd $SCRIPTDIR
	cp clumpyMC.* $ODIR/Inputfiles/
	rm $ODIR/Inputfiles/clumpyMC.runparameter
		
	sed	-e "s|PEEED|$QLOG|" \
		-e "s|OODIR|$ODIR|" \
		-e "s|DDDDD|$DATE|" \
		-e "s|NSSSE|$NSIDE|" \
		-e "s|SSSSS|$sigmaFOVdeg|" \
		-e "s|PPPPP|$psi0deg|" \
		-e "s|TTTTT|$theta0deg|" \
		-e "s|ROOOI|$inputfile|" \
		-e "s|PSSSF|$sigmaPSFdeg|" \
		-e "s|NNNNN|$nevents|" \
		-e "s|FFFFF|$fDM|" \
		-e "s|SIIIG|$fsig|" \
		-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh
		
	chmod u+x $FNAM.sh
	echo $FNAM.sh

	qsub -V -j y -m bea -l h_cpu=47:59:00 -l h_rt=47:59:00 -l os=sl6 -l h_vmem=8G -l tmpdir_size=8G -o $QLOG/ -e $QLOG/ "$FNAM.sh"

fi

