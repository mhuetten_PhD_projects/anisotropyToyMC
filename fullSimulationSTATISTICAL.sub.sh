#!/bin/sh
#
# script run clumpy simulations:
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
   echo
   echo "Submission script for multiple skymap clumpy simulation for statistical evaluation:"
   echo "For a given number of identical simulations,"
   echo "- firstly, a clumpy skymap is computed with multipole evaluation"
   echo "- secondly, a Detector Monte Carlo is run with again a multiploe evaluation"
   echo
   echo "fullSimulationSTATISTICAL.sub.sh <dmClumps.parameters> <clumpyMC.parameters> [number of simulations]"
   echo
   echo "or"
   echo
   echo "fullSimulationSTATISTICAL.sub.sh <dmClumps.parameters> <clumpyMC.parameters> [number of simulations] [dmClumps.runparameter]" 
   echo
   echo "  for the parameter files use template without changing line numbers"
   echo "  in case of choosing a varying parameter in a dmClumps.runparameter file,"
   echo "  write name of parameter in first line, then al values you want to compute."
   echo "  Example:"
   echo
   echo "    user_rse"
   echo "    5.0"
   echo "    10.0"
   echo "    15.0"
   echo

   exit
fi

#read variables from dmClumps.parameters file:
# run name and folde ris taken from that file

# General parameters:
export runname=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $1 		| head -n15 | tail -n1 | sed -e 's/^[ \t]*//') 
export alphaIntDeg=$(cat $1 	| head -n18 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $1 		| head -n24 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $1 	| head -n27 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $1 	| head -n30 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $1 	| head -n33 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $1 | head -n39 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $1 	| head -n45 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $1 | head -n48 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $1 | head -n49 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $1 | head -n50 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $1 		| head -n55 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $1 			| head -n58 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $1 				| head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $1 				| head -n64 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $1 	 | head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $1 | head -n73 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $1 | head -n74 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $1 | head -n75 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $1		 | head -n78 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDM_SLOPE=$(cat $1			 | head -n81 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $1			 | head -n84 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $1		 | head -n87 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $1   | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $1 | head -n96 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $1 | head -n97 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $1 | head -n98 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $1  | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')

# Additional parameters:
export gDM_MMAXFRAC_SUBS=$(cat $1 | head -n107 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_RHOSAT=$(cat $1  | head -n110 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M1=$(cat $1 | head -n113 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M2=$(cat $1  | head -n116 | tail -n1 | sed -e 's/^[ \t]*//')



#read variables from clumpyMC.parameters file:

# General parameters:

export NSIDE=$(cat $2 		| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated telescope properties:
export sigmaFOVdeg=$(cat $2 | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')
#psi0deg=$psiZeroDeg
#theta0deg=$(( 90 - $thetaZeroDeg )) 
export sigmaPSFdeg=$(cat $2 | head -n35 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated physics:
export nevents=$(cat $2 	| head -n41 | tail -n1 | sed -e 's/^[ \t]*//')
export fsig=$(cat $2 		| head -n44 | tail -n1 | sed -e 's/^[ \t]*//')
export fDM=$(cat $2 		| head -n47 | tail -n1 | sed -e 's/^[ \t]*//')

###############
#echo $runname
#echo $outdir
#echo $user_rse
#echo $alphaIntDeg
#echo $psiZeroDeg
#echo $thetaZeroDeg
#echo $psiWidthDeg
#echo $thetaWidthDeg
#echo $gSIMU_IS_ANNIHIL_OR_DECAY
#echo $gGAL_TOT_FLAG_PROFILE
#echo $gGAL_TOT_SHAPE_PARAMS_0
#echo $gGAL_TOT_SHAPE_PARAMS_1
#echo $gGAL_TOT_SHAPE_PARAMS_2
#echo $gGAL_TOT_RSCALE
#echo $gGAL_RHOSOL
#echo $gGAL_RSOL
#echo $gGAL_RVIR
#echo $gGAL_DPDV_FLAG_PROFILE
#echo $gGAL_DPDV_SHAPE_PARAMS_0
#echo $gGAL_DPDV_SHAPE_PARAMS_1
#echo $gGAL_DPDV_SHAPE_PARAMS_2
#echo $gGAL_DPDV_RSCALE
#echo $gGAL_DPDM_SLOPE
#echo $gDM_MMIN_SUBS
#echo $gGAL_SUBS_N_INM1M2
#echo $gGAL_CLUMPS_FLAG_PROFILE
#echo $gGAL_CLUMPS_SHAPE_PARAMS_0
#echo $gGAL_CLUMPS_SHAPE_PARAMS_1
#echo $gGAL_CLUMPS_SHAPE_PARAMS_2
#echo $gGAL_CLUMPS_FLAG_CVIRMVIR
#############

# number of samples:
samplenumber=$3


if [ -n "$4" ]
then
	PLIST=$4
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
	echo
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"
# now forget outdir

# skeleton script
FSCRIPT="fullSimulationSTATISTICAL.qsub"

# If a runparameter file exists:
if [ -n "$4" ]
then
	cd $RUNDIR
	rm -r Inputfiles
	mkdir -p Inputfiles
	cd $SCRIPTDIR
	cp dmClumps.* $RUNDIR/Inputfiles/
	cp clumpyMC.* $RUNDIR/Inputfiles/
	cp fullSimulationSTATISTICAL.* $RUNDIR/Inputfiles/
	rm $RUNDIR/Inputfiles/dmClumps.clumpy_params.txt

# now loop over all files in files loop:

for AFIL in $PARAMETERS
do
   echo "now starting parameter run $runparameter = $AFIL"

   # create output directories:   
   cd $RUNDIR
   mkdir $runparameter-$AFIL
   cd $runparameter-$AFIL
   ODIR="$(pwd)"
   rm -r Logfiles
   mkdir -p Logfiles
   mkdir -p Inputfiles
   cd $SCRIPTDIR
   cp dmClumps.* $RUNDIR/Inputfiles/
   rm $RUNDIR/Inputfiles/dmClumps.clumpy_params.txt
   rm $RUNDIR/Inputfiles/clumpyMC.runparameter


   QLOG=$ODIR/Logfiles

   for i in $(seq 1 1 $samplenumber)
   do
   echo "now starting sample realisation number $i"

   FNAM="$QLOG/fullSimulationSTATISTICAL-$runname-$runparameter-$AFIL-$i"

   # output directory for error/output from batch system
   # in case you submit a lot of scripts: QLOG=/dev/null

   case $runparameter in
	"user_rse") user_rse=$AFIL;;
	"alphaIntDeg") alphaIntDeg=$AFIL;;
	"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$AFIL;;
	"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$AFIL;;
	"gGAL_RHOSOL") gGAL_RHOSOL=$AFIL;;
	"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$AFIL;;
	"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$AFIL;;
	"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$AFIL;;
	"nevents") nevents=$AFIL;;
	"fsig") fsig=$AFIL;;
	"fDM") fDM=$AFIL;;
   esac

   sed -e "s|FFFFF|$user_rse|" \
       -e "s|PEEED|$QLOG|" \
       -e "s|OODIR|$ODIR|" \
       -e "s|DAATE|$DATE|" \
       -e "s|MMMMM|$gGAL_RHOSOL|" \
       -e "s|NNNNN|$gGAL_RSOL|" \
       -e "s|OOOOO|$gGAL_RVIR|" \
       -e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
       -e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
       -e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
       -e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
       -e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
       -e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
       -e "s|WWWWW|$gDM_MMIN_SUBS|" \
       -e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
       -e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
       -e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
       -e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
       -e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
       -e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
       -e "s|AAAAA|$psiZeroDeg|" \
       -e "s|BBBBB|$thetaZeroDeg|" \
       -e "s|CCCCC|$psiWidthDeg|" \
       -e "s|DDDDD|$thetaWidthDeg|" \
       -e "s|EEEEE|$alphaIntDeg|" \
       -e "s|RRRRR|$AFIL|" \
       -e "s|RUUUN|$RUNDIR|" \
       -e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
       -e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
       -e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
       -e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
       -e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
       -e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
       -e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
       -e "s|LLLLL|$gGAL_TOT_RSCALE|" \
       -e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
       -e "s|ZEEEE|$gDM_RHOSAT|" \
       -e "s|ZFFFF|$gGAL_SUBS_M1|" \
       -e "s|ZGGGG|$gGAL_SUBS_M2|" \
       -e "s|ZHHHH|$NSIDE|" \
       -e "s|ZIIII|$sigmaFOVdeg|" \
       -e "s|ZJJJJ|$sigmaPSFdeg|" \
       -e "s|ZKKKK|$nevents|" \
       -e "s|ZLLLL|$fsig|" \
       -e "s|ZMMMM|$fDM|" \
       -e "s|ZNNNN|$i|" \
       -e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh

   chmod u+x $FNAM.sh
   echo $FNAM.sh

qsub -V -j y -m a -l h_cpu=47:59:00 -l h_rt=47:59:00  -l os=sl6 -l h_vmem=12G -l tmpdir_size=12G -o $QLOG/ -e $QLOG/ "$FNAM.sh"
#qsub -V -j y -m a -l h_cpu=00:29:00 -l h_rt=00:29:00  -l os=sl6 -l h_vmem=4G -l tmpdir_size=4G -o $QLOG/ -e $QLOG/ "$FNAM.sh"
   echo
   done
done
fi

if [ ! -n "$4" ]
then
   echo "now submitting single job to batch"
# RUNDIR is equal ODIR in this case!
   
AFIL=singlerundummy

# AFIL is not used in this case!

   cd $RUNDIR
   ODIR="$(pwd)"
   rm -r Logfiles
   rm -r Inputfiles

   mkdir -p Inputfiles
   mkdir -p Logfiles
   QLOG=$ODIR/Logfiles
   

   cd $SCRIPTDIR
   cp dmClumps.* $ODIR/Inputfiles/
   cp clumpyMC.* $RUNDIR/Inputfiles/
   cp fullSimulationSTATISTICAL.* $RUNDIR/Inputfiles/
   rm $ODIR/Inputfiles/dmClumps.clumpy_params.txt
   rm $ODIR/Inputfiles/*.runparameter

for i in  $(seq 1 1 $samplenumber)
   do
   echo "now starting sample realisation number $i"
    realisationnumber=$i # this rename has just benn done because of some buggy reading of the sed command...

   FNAM="$QLOG/fullSimulationSTATISTICAL-$runname-$i"


   sed -e "s|FFFFF|$user_rse|" \
       -e "s|PEEED|$QLOG|" \
       -e "s|OODIR|$ODIR|" \
       -e "s|DAATE|$DATE|" \
       -e "s|MMMMM|$gGAL_RHOSOL|" \
       -e "s|NNNNN|$gGAL_RSOL|" \
       -e "s|OOOOO|$gGAL_RVIR|" \
       -e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
       -e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
       -e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
       -e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
       -e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
       -e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
       -e "s|WWWWW|$gDM_MMIN_SUBS|" \
       -e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
       -e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
       -e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
       -e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
       -e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
       -e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
       -e "s|AAAAA|$psiZeroDeg|" \
       -e "s|BBBBB|$thetaZeroDeg|" \
       -e "s|CCCCC|$psiWidthDeg|" \
       -e "s|DDDDD|$thetaWidthDeg|" \
       -e "s|EEEEE|$alphaIntDeg|" \
       -e "s|RRRRR|$AFIL|" \
       -e "s|RUUUN|$RUNDIR|" \
       -e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
       -e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
       -e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
       -e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
       -e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
       -e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
       -e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
       -e "s|LLLLL|$gGAL_TOT_RSCALE|" \
       -e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
       -e "s|ZEEEE|$gDM_RHOSAT|" \
       -e "s|ZFFFF|$gGAL_SUBS_M1|" \
       -e "s|ZGGGG|$gGAL_SUBS_M2|" \
       -e "s|ZHHHH|$NSIDE|" \
       -e "s|ZIIII|$sigmaFOVdeg|" \
       -e "s|ZJJJJ|$sigmaPSFdeg|" \
       -e "s|ZKKKK|$nevents|" \
       -e "s|ZLLLL|$fsig|" \
       -e "s|ZMMMM|$fDM|" \
       -e "s|ZNNNN|$realisationnumber|" \
       -e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh

  chmod u+x $FNAM.sh
 echo $FNAM.sh

qsub -V -j y -m a -l h_cpu=47:59:00 -l h_rt=47:59:00  -l os=sl6 -l h_vmem=12G -l tmpdir_size=12G -o $QLOG/ -e $QLOG/ "$FNAM.sh"
#qsub -V -j y -m a -l h_cpu=00:29:00 -l h_rt=00:29:00  -l os=sl6 -l h_vmem=2G -l tmpdir_size=2G -o $QLOG/ -e $QLOG/ "$FNAM.sh"
   echo
   done
fi
exit
