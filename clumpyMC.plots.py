# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
## give default variables:
# grid resolution:
# NSIDE = 2**9
# # gaussian width of fov in degs:
# sigmaFOVdeg = 2.5
# # center of fov on celestial sphere:
# phi0deg =  0.0
# theta0deg = 0.0
# # ROI width:
# ROIwidthfactor = 10
# # gaussian width of point spread function:
# sigmaPSFdeg = 0.1
# # number of simulated events:
# nevents = 10**4
# # ratio signal events to isotropic background noise events:
# # fsig = N_sig/(N_sig + N_back)
# fsig = 0.0
# # power spectrum index for simulated skymaps:
# s = 2.0
# # ratio DM induced events to astrophysical events:
# fDM = 0.0
# # runnumber:
# runnumber = "testlocal5"
# output directory:
directory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"

# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:f:p:l:w:v:t:e:g:d:i:o:",["nside=","fov=","psf=","psi=","theta=","psiWidthDeg=","thetaWidthDeg=","events=","fsig=","fDM=","infile=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -l <psi0deg>        (no standard)'
    print ' -t <theta0deg>      (no standard)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -i <input file>     (no standard)'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -l <psi0deg>        (no standard)'
        print ' -t <theta0deg>      (no standard)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -i <input file>     (no standard)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-l", "--psi"):
        psi0deg = float(arg)
    elif opt in ("-t", "--theta"):
        theta0deg = float(arg)
    elif opt in ("-w", "--psiWidthDeg"):
        psiWidthDeg = float(arg)
    elif opt in ("-v", "--thetaWidthDeg"):
        thetaWidthDeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-i", "--infile"):
        inputfile = arg
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg

# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)


## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)

theta0degLon = theta0deg # old runs: 90 - theta0deg
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)*2.0/np.sqrt(np.pi) # compare with healpix documentation: this calculation gives the full diameter of a circular assumed region.
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*resolution
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars


# give out input variables:
print  " "
print  "** INPUT PARAMETERS: **"
print "NSIDE: ",NSIDE
print "Number of grid pixels on whole sphere: ",npix
print "Using gaussian FOV acceptance..."
print "Full diameter of FOV acceptance (1 sigma): ",sigmaFOVdeg,"°"
print "Full diameter of gaussian PSF (1 sigma): ",sigmaPSFdeg,"°" 
print "Center position in sky in celestial coord, latitude: ",theta0degLon,"°" 
print "Center position in sky in celestial coord, longitude ",psi0deg,"°" 
print "Resolution of skymap in degs: ",resolutiondeg, "°"
print "Number of MC events: ",nevents
print "Ratio fsig: ",fsig
print "Ratio fDM: ",fDM
print "Maximum resolvable multipole index l (lmax): ",lmax
print "** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS ADD FLAG -h AT STARTING **"
print  " "


filename = "plots"
outputname_plots = str(outputdirectory)+"/%s.pdf" % filename 
outputfile_plots = pdf(outputname_plots)



print "Saving plot of skymap..."
  
#Load skymap:
filename = "skymap"
inputname_skymap = str(outputdirectory)+"/%s.healpix" % filename
 
inputskymapData = np.loadtxt(inputname_skymap)
 
npixROI = len(inputskymapData[:,0])
npixROI = int(npixROI) 
# Constrain calculation to ROI:
ROIpixelsIN = inputskymapData[:,0]
ROIpixels = np.zeros(npixROI)
  
for i in range(npixROI):
    ROIpixels[i] = int(float(ROIpixelsIN[i]))
 
skymapData = inputskymapData[:,1]
 
skymap = np.zeros(npix)-1.6375*10**(30)
 
for ROIpixelnr in range(npixROI):
     skymap[ROIpixels[ROIpixelnr]] = skymapData[ROIpixelnr]
 
  
print " maximum value in skymap:", round(max(skymap),3)
  
# plot skymap:
plot_skymap = plt.figure(figsize=(9,10))
plt.axis('off')
TITLE = 'skymap at coordinates (lon=%s+/-%s degs, lat=%s+/-%s degs),\n  resolution = %s degs.'%(psi0deg,psiWidthDeg*0.6,theta0degLon,thetaWidthDeg*0.6,round(resolutiondeg,4))
#plt.title('\n'.join(wrap(title,85)))
#plt.subplots_adjust(top=0.95)  
#hp.mollview(skymap,nest=False, fig=1, unit='events per pixel', title='',rot=[psi0deg,theta0degLon], flip='geo') 
hp.cartview(skymap, fig=1,rot=[psi0deg,theta0degLon], flip='geo', lonra= [-psiWidthDeg*0.6,psiWidthDeg*0.6], latra= [-thetaWidthDeg*0.6,+thetaWidthDeg*0.6],  title=TITLE, unit='normalized probability')
  
hp.graticule(dpar=5, dmer=5)
outputfile_plots.savefig(plot_skymap)



print "Saving plot of eventmap..."

#Load eventmap:
filename = "eventmap"
inputname_eventmap = str(outputdirectory)+"/%s.healpix" % filename 

inputeventmapData = np.loadtxt(inputname_eventmap)

npixROI = len(inputeventmapData[:,0])
npixROI = int(npixROI) 
# Constrain calculation to ROI:
ROIpixelsIN = inputeventmapData[:,0]
ROIpixels = np.zeros(npixROI)
  
for i in range(npixROI):
    ROIpixels[i] = int(float(ROIpixelsIN[i]))

eventmapData = inputeventmapData[:,1]

eventmap = np.zeros(npix)-1.6375*10**(30)

for ROIpixelnr in range(npixROI):
     eventmap[ROIpixels[ROIpixelnr]] = eventmapData[ROIpixelnr]


print " maximum value in eventmap:", round(max(eventmap),3), "events/pixel"
[postheta, pospsi] = hp.pix2ang(npix,int(np.argmax(eventmap)))
print " location of maximum value in eventmap:",pospsi, postheta
# plot event map:
plot_eventmap = plt.figure(figsize=(9,10))
plt.axis('off')
TITLE = 'Event skymap for total events n = %s, signal-to-noise ratio = %s,\n Gaussian FOV = %s degs., PSF = %s degs., DM ratio = %s,  resolution = %s degs.'%(nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,round(resolutiondeg,4))
#plt.title('\n'.join(wrap(title,85)))
#plt.subplots_adjust(top=0.95)  
#hp.mollview(eventmap,nest=False, fig=2, unit='events per pixel', title='',rot=[theta0degLon,psi0deg], flip='geo')
hp.cartview(eventmap, fig=2,rot=[psi0deg,theta0degLon], flip='geo', lonra= [-psiWidthDeg*0.6,psiWidthDeg*0.6], latra= [-thetaWidthDeg*0.6,+thetaWidthDeg*0.6],  title=TITLE, unit='normal. events/pixel')

hp.graticule(dpar=5, dmer=5)
outputfile_plots.savefig(plot_eventmap)



# print "Saving plots of 2d histogram..."
# thetamax = 5 * sigmaFOV # in deg
# thetapixelnr = 2*int(thetamax/resolution)+1 # always an odd number
# deltatheta = 2*thetamax/(thetapixelnr-1) # in deg
# theta = np.zeros(thetapixelnr)
# profile = np.zeros(thetapixelnr)
# pixelnr = np.zeros(thetapixelnr)
# gausscurve = np.zeros(thetapixelnr)
# for i in range(thetapixelnr):
#     theta[i] = -thetamax + i * deltatheta
#     if theta[i]<0:
#         theta[i] = -theta[i]
#         phi0lat = phi0lat +np.pi
#     pixelnr[i] = hp.ang2pix(NSIDE, theta0 + theta[i], phi0lat,nest=False)
#     profile[i] = eventmap[pixelnr[i]]
# for i in range((thetapixelnr-1)/2):
#     theta[i] = i * deltatheta
#     gausscurve[(thetapixelnr-1)/2-i] = max(profile)*np.exp(-0.5*theta[i]**2/(sigmaFOV )**2) 
#     gausscurve[i+(thetapixelnr-1)/2] = max(profile)*np.exp(-0.5*theta[i]**2/(sigmaFOV )**2)
#     theta[i] = -thetamax + i * deltatheta
# deltathetadeg = deltatheta*180/np.pi
# thetadeg = 180*theta/np.pi      
# plot_eventmapprofile = plt.figure(figsize=(9, 7))
# plt.bar(thetadeg, profile , width=deltathetadeg,  facecolor='g')    
# plt.plot(thetadeg,gausscurve,'b',linewidth=1.5)
# plt.xlabel(' Delta theta in degrees (theta =  theta_0 +- Delta theta)')
# plt.ylabel(' event counts per pixel')
# title ='Profile of normalized event skymap at  phi=phi_0  for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s degs., PSF = %s degs., DM ratio = %s, s = %s, resolution = %s degs, # of slots: %s, delta theta = %s degs.'%(nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,s,round(resolutiondeg,3),thetapixelnr,round(deltathetadeg,3))
# plt.title('\n'.join(wrap(title,90)))
# plt.subplots_adjust(top=0.87) 
# outputfile_plots.savefig(plot_eventmapprofile)
# plt.close()
  

print "Saving plots of output power spectrum..."


#Load output spectrum:
filename = "clOutput"
inputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 


clspectrum = pf.open(inputname_clOutput)

info = clspectrum.info()

print info
print ""
header = pf.getheader(inputname_clOutput)
print header

print header.keys()

data = pf.getdata(inputname_clOutput)

print len(data)


clOUT = data.field(0)
clOUT = clOUT[1:]


plot_powerspectrum = plt.figure(figsize=(9, 6))
ell = np.arange(len(clOUT))

#plt.plot(ell, (ell) * (ell+1) * clsim/(2*np.pi),color='b')# 
plt.plot(ell, ell * (ell+1) * clOUT/(2*np.pi),color='r')#
#plt.plot(ell, ell * (ell+1) * clSIM/(2*np.pi),color='k')#
#plt.plot(ell, ell * (ell+1) * clSIMnoise/(2*np.pi),'k--')#
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
plt.xscale('log')
plt.yscale('log')
pylab.xlim([1,lmax])
pylab.ylim([10**(-4),10**6])
title = 'Angular power spectrum from normalized event skymap for total events n = %s,  sig. to noise ratio = %s, Gaussian FOV = %s degs., DM ratio = %s,  resolution = %s degs.'%(nevents,fsig,sigmaFOVdeg,fDM,round(resolutiondeg,3))
plt.title('\n'.join(wrap(title,80)))
plt.subplots_adjust(top=0.85)
#plt.fill_between(ell, ell * (ell+1) * cl/(2*np.pi),ell * (ell+1) * cl2/(2*np.pi),color='r',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * cl/(2*np.pi),ell * (ell+1) * cl3/(2*np.pi),color='r',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * cl2/(2*np.pi),ell * (ell+1) * cl3/(2*np.pi),color='r',alpha=1)
 
outputfile_plots.savefig(plot_powerspectrum)

outputfile_plots.close()

#plt.show()

print "finished."
print ""

