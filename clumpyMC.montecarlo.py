#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import time
import os
import sys
import getopt
import pyfits


# read input variables
try:
	opts, args = getopt.getopt(sys.argv[1:],"hn:f:p:l:t:e:g:d:i:o:",["nside=","fov=","psf=","psi=","theta=","events=","fsig=","fDM=","filein=","dirout="])
except getopt.GetoptError:
	# This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
	# The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
	print 'The input options are:'
	print ' -n <NSIDE>		  (Standard: 2^9)'
	print ' -f <sigmaFOVdeg>	(Standard: 2.5°)'
	print ' -p <sigmaPSFdeg>	(Standard: 0.1°)'
	print ' -l <psi0deg>		(no standard)'
	print ' -t <theta0deg>	  (no standard)'
	print ' -e <nevents>		(Standard: 10^4)'
	print ' -g <fsig>		   (Standard: 0.0)'
	print ' -d <fDM>			(Standard: 0.0)'
	print ' -i <input file>	 (no standard, ends with .healpix)'
	print ' -o <output dir.>	(Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
	sys.exit(2)
for opt, arg in opts:
	if opt == '-h':
		# help option
		print 'The input options are:'
		print ' -n <NSIDE>		  (Standard: 2^9)'
		print ' -f <sigmaFOVdeg>	(Standard: 2.5°)'
		print ' -p <sigmaPSFdeg>	(Standard: 0.1°)'
		print ' -l <psi0deg>		(no standard)'
		print ' -t <theta0deg>	  (no standard)'
		print ' -e <nevents>		(Standard: 10^4)'
		print ' -g <fsig>		   (Standard: 0.0)'
		print ' -d <fDM>			(Standard: 0.0)'
		print ' -i <input file>	 (no standard, ends with .healpix)'
		print ' -o <output dir.>	(Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
		sys.exit()
	elif opt in ("-n", "--nside"):
		NSIDE = int(arg)
	elif opt in ("-f", "--fov"):
		sigmaFOVdeg = float(arg)
	elif opt in ("-p", "--psf"):
		sigmaPSFdeg = float(arg)
	elif opt in ("-l", "--psi"):
		psi0deg = float(arg)
	elif opt in ("-t", "--theta"):
		theta0deg = float(arg)
	elif opt in ("-e", "--events"):
		nevents = int(arg)
	elif opt in ("-g", "--fsig"):
		fsig = float(arg)
	elif opt in ("-d", "--fDM"):
		fDM = float(arg)
	elif opt in ("-i", "--filein"):
		inputfile = arg 
	elif opt in ("-o", "--dirout"):
		outputdirectory = arg


start_time = time.time()
## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)
# give celestial position in rads:
psi0 =  np.pi*psi0deg/180.0
psi0lat = -psi0 # healpy counts phi clockwise!!
theta0deg=90.0-theta0deg
theta0 = np.pi*theta0deg/180.0
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)
lmax = int(np.pi/hp.nside2resol(NSIDE))


print " Load skymap..."

inputClumpyData = np.loadtxt(inputfile)

npixROI = len(inputClumpyData[:,0])
npixROI = int(npixROI) 
# Constrain calculation to ROI:
ROIpixelsIN = inputClumpyData[:,0]

ROIpixels = np.zeros(npixROI)
 
for i in range(npixROI):
	ROIpixels[i] = int(float(ROIpixelsIN[i]))


print " "
print " Generate gaussian mask of field of view..."


FOVmask = np.zeros(npixROI) # gaussian weighted mask of field of view

for ROIpixelnr in range(npixROI):
	angles = hp.pix2ang(NSIDE, int(ROIpixels[ROIpixelnr]))
	thetaFOV  = angles[0]
	psiFOV = angles[1]
	#print angles
	# here, the shape of the acceptance is defined:
	#print (np.arccos(np.sin(theta0-np.pi/2)*np.sin(thetaFOV-np.pi/2) + np.cos(theta0-np.pi/2)*np.cos(thetaFOV-np.pi/2)*np.cos(psi0-psiFOV)))**2/(sigmaFOV)**2
	FOVmask[ROIpixelnr] =  np.e**(-0.5*(np.arccos(np.sin(theta0-np.pi/2)*np.sin(thetaFOV-np.pi/2) + np.cos(theta0-np.pi/2)*np.cos(thetaFOV-np.pi/2)*np.cos(psi0-psiFOV)))**2/(sigmaFOV)**2)

print " FOVmask filled after", round((time.time() - start_time)/60,2), "minutes." 
 
# FOVmap = np.zeros(npix)
# for ROIpixelnr in range(npixROI):
#	 FOVmap[ROIpixels[ROIpixelnr]] = FOVmask[ROIpixelnr]


eventmap = np.zeros(npix) # simulated event skymap  containing background and signal events

skymap = inputClumpyData[:,5]
skymap = (skymap-min(skymap))/(max(skymap)-min(skymap))
 
print " Maximum value in skymap: ", max(skymap)
print  " Minimum value in skymap: ",min(skymap)
 
skymapmap = np.zeros(npix)-1.6375*10**(30)

for ROIpixelnr in range(npixROI):
	 skymapmap[ROIpixels[ROIpixelnr]] = skymap[ROIpixelnr]

 
print " Saving skymap to file..."
 
# Create Skymap Filename:
filename = "skymap"
outputpath_skymap = str(outputdirectory)+"/%s.healpix" % filename 


f = open(outputpath_skymap, 'w')
f.write("# number of pixels in healpix map: ")
f.write(str(npix))
f.write("\n")   
f.write("#	 healpix map pixel #	|  	Jtot")
f.write("\n") 
f.write("#				  |						 [Msol^2/kpc^5/sr]")
f.write("\n")

for ROIpixelnr in range(npixROI):
			f.write(str(ROIpixels[ROIpixelnr]))
			f.write(" ")
			f.write(str(skymap[ROIpixelnr]))
			f.write("\n")

f.close()
skymapRED = skymap # skymap reduced to ROI
# for ROIpixelnr in range(npixROI):
#	 skymapRED[ROIpixelnr] = skymap[ROIpixelnr] #skymap[ROI[ROIpixelnr]]
del skymap

eventnumber = 0 # to count events from zero to nevents
backgroundcounts = 0 # to later check ratio between signal and background events.
signalcounts = 0  
 



print " "
print " Now start Monte Carlo: Fill eventmap with events..."

# create logfile that is written out continously during program running on batch
filename = "logfile"
outputname_logfile = str(outputdirectory)+"/%s.txt" % filename 


while eventnumber<nevents:
	eventbool = 0
	# first choose if signal or background event:
	randomnumber1 = rn.randint(0, 10.0*nevents)/(10.0*nevents)
	if randomnumber1>fsig:
			# it is a background event, now select arbitrary pixel within the ROI:
			randomROIpixelnr = rn.randint(0, npixROI-1)
			#randompixelnr = ROI[randomROIpixelnr] # will be needed to fill eventmap
			# the event has not yet detected, still has to pass acceptance function of fov:
			randomnumber3 = rn.randint(0, 10*nevents)/(10.0*nevents)
			if randomnumber3<FOVmask[randomROIpixelnr]:
				# o.k., has succesfully passed the acceptance window, now it really is detected.
				backgroundcounts = backgroundcounts + 1
				eventbool = 1				 
				#info = " background!"
	else:
			# It's a signal event! Now, don't select pixel uniformly distributed within the ROI,
			# but according to the probability skymap:
			signalbool = 0
			while signalbool==0:
				randomROIpixelnr = rn.randint(0, npixROI-1)
				#randompixelnr = ROI[randomROIpixelnr] # willl be needed to fill eventmap
				randomnumber4 = rn.randint(0, 10*nevents)/(10.0*nevents)
				if randomnumber4<skymapRED[randomROIpixelnr]:
					signalbool = 1
					# great, at point [pixelnr] really a signal event takes place, but again has to 
					# pass the acceptance function:
					randomnumber3 = rn.randint(0, 10*nevents)/(10.0*nevents)
					if randomnumber3<FOVmask[randomROIpixelnr]:
						# o.k., has succesfully passed the acceptance window, now it really is detected!
						signalcounts = signalcounts + 1
						eventbool = 1
						#info = " signal!"
	# now finally count event:
	if eventbool==1:
			## count event at pixel "randompixelnr", but still displace by PSF:
			# get angular position of event:
			angles = hp.pix2ang(NSIDE, int(ROIpixels[randomROIpixelnr]),nest=False) #randomROIpixelnr ROIpixels[ROIpixelnr]
			thetaEVENT  = angles[0]
			phiEVENT = -angles[1]
			#get radial coordinate of displacement by a gaussian distribution:
			gauss1 = rn.gauss(0, sigmaPSF)
			gauss2 = rn.gauss(0, sigmaPSF)
			#get angular distance of displacement:
			d = np.sqrt(gauss1**2 + gauss2**2)
			# correct sign lost by arcustangens:
			if gauss1<0: 
				philocal =  np.arctan(gauss2/gauss1)+np.pi
			else:
				philocal =  np.arctan(gauss2/gauss1)
			# now apply rotation matrix to displace event by distance d, angle philocal not from the north pole, but from theta_event, phi_event
			x  = np.sin(phiEVENT) * np.sin(d) * np.cos(philocal) - np.cos(thetaEVENT) * np.cos(phiEVENT) * np.sin(d) * np.sin(philocal) + np.sin(thetaEVENT) * np.cos(phiEVENT) * np.cos(d) 
			y  = np.cos(phiEVENT) * np.sin(d) * np.cos(philocal) + np.cos(thetaEVENT) * np.sin(phiEVENT) * np.sin(d) * np.sin(philocal) - np.sin(thetaEVENT) * np.sin(phiEVENT) * np.cos(d)
			z  =												   np.sin(thetaEVENT) *					np.sin(d) * np.sin(philocal) + np.cos(thetaEVENT) *					np.cos(d) 
			randompixelnr = hp.vec2pix(NSIDE,x ,y ,z , nest=False)
		  
			eventmap[randompixelnr] = eventmap[randompixelnr] + 1
			eventnumber = eventnumber + 1
			# print out computation times:
			if eventnumber in [0.1*nevents, 0.2*nevents, 0.3*nevents, 0.4*nevents, 0.5*nevents, 0.6*nevents, 0.7*nevents, 0.8*nevents, 0.9*nevents]:
				print int(eventnumber*100/nevents), "% done after", round((time.time() - start_time)/60,2), "minutes."
				# write out parallelly status to continously updated logfile:
				outputfile_logfile = open(outputname_logfile, 'a')
				outputfile_logfile.write(str(int(eventnumber*100/nevents)))
				outputfile_logfile.write("% done after ")
				outputfile_logfile.write(str( round((time.time() - start_time)/60,2)))
				outputfile_logfile.write(" minutes.\n")
				outputfile_logfile.close()
 
del FOVmask			 
 
print " Eventmap filled with total", nevents, "events." 
print " Signal events: ", signalcounts
print " Background events: ", backgroundcounts
print " "


# smoothe eventmap with PSF:
#print "Smoothing eventmap within PSF..."
#eventmap = hp.smoothing(eventmap, fwhm=sigmaPSF)
 
 
# normalize eventmap:
eventmap = eventmap*npix/nevents
  
print " Saving eventmap to file..."
 
 
# Create eventmap Filename:
filename = "eventmap"

outputpath_eventmap = str(outputdirectory)+"/%s.healpix" % filename 

outputpath_eventmap_fits = str(outputdirectory)+"/%s.fits" % filename 
hp.write_map(outputpath_eventmap_fits, eventmap)


f = open(outputpath_eventmap, 'w')
f.write("# number of pixels in healpix map: ")
f.write(str(npix))
f.write("\n")   
f.write("#	 healpix map pixel #	|  	normalized event numbers")
f.write("\n") 


for ROIpixelnr in range(npixROI):
			f.write(str(ROIpixels[ROIpixelnr]))
			f.write(" ")
			f.write(str(eventmap[ROIpixels[ROIpixelnr]]))
			f.write("\n")

f.close() 
 
del eventmap


## compute power spectrum:
# Create Output Spectrum Filename:
filename = "clOutput"
outputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 


print " Write Anafast parameter file anafast.par..."
# Create Anafast Input parameter file:
filename =  "anafast"
outputname_anafastpar = str(outputdirectory)+"/%s.par" % filename 
outputfile_anafastpar = open(outputname_anafastpar, 'wb')


# Fill Anafast parameter file Anafast.par:
outputfile_anafastpar.write("simul_type = 1\n\n")
outputfile_anafastpar.write("nlmax = ")
outputfile_anafastpar.write(str(lmax))
outputfile_anafastpar.write("\n\n")
outputfile_anafastpar.write("infile = ")
outputfile_anafastpar.write(outputpath_eventmap_fits)
outputfile_anafastpar.write("\n\n") 
outputfile_anafastpar.write("maskfile = ''\n\n")
outputfile_anafastpar.write("theta_cut_deg = 0.00\n\n")
outputfile_anafastpar.write("regression = 1\n\n") # removes monopole term
outputfile_anafastpar.write("plmfile = ''\n\n")
outputfile_anafastpar.write("outfile = ")
outputfile_anafastpar.write(outputname_clOutput)
outputfile_anafastpar.write("\n\n")
outputfile_anafastpar.write("outfile_alms = ''\n\n") #outfile_alms=$ODIR/${SPARAMETER}_alm.fits 
outputfile_anafastpar.write("won = 0\n\n")
outputfile_anafastpar.write("iter_order = 0")
outputfile_anafastpar.close()

print " Script clumpyMC.montecarlo.py successfully finished."
