import numpy as np


import pickle
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.

import healpy as hp
import time



start_time = time.time()
NSIDE = 2**11

#gaussian width of fov in degree:
sigmaFOVdeg = 2.5
# in radians:
sigmaFOV = np.pi*sigmaFOVdeg/180
# width of ROI:
ROIwidthfactor = 10
ROIwidth = ROIwidthfactor*sigmaFOV
# center of fov:
phi0 =  0
theta0 = (np.pi/2)



# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)


print "NSIDE"
print NSIDE


# give out input variables:
npix=hp.nside2npix(NSIDE)
print "npix"
print npix
print "fov in degree"
print sigmaFOV*180/np.pi
print "resolution of skymap in degrees:"
print 180/np.pi*hp.nside2resol(NSIDE)


# create neccessary fields:
ROI = [] # vector, will be filled with pixel indexes of interest


## simple calculation of pixelnr_max:
theta = 0
pixelnr = 0
while theta<ROIwidth/2:
    angles = hp.pix2ang(NSIDE, pixelnr,nest=False)
    theta = angles[0]
    ROI.append(pixelnr)
    pixelnr = pixelnr + 1
    
    if pixelnr in [1,np.ceil(0.001*npix),np.ceil(0.01*npix), np.ceil(0.05*npix),np.ceil(0.1*npix),np.ceil(0.2*npix),np.ceil(0.3*npix),np.ceil(0.4*npix),np.ceil(0.5*npix),np.ceil(0.6*npix),np.ceil(0.7*npix),np.ceil(0.8*npix),np.ceil(0.9*npix)]:
        print int(pixelnr*100/npix), "% of total pixels scanned in", round((time.time() - start_time)/60,2), "minutes"
thetaMAX = theta
pixelnrMAX = pixelnr

print thetaMAX/np.pi*180.0, pixelnrMAX
##########################
# new algorithm:
# create ROI around north pole, then rotate every ROI pixel to desired longitude/latitude:


# theta = 0
# pixelnr = 0
# 
# while theta<ROIwidth/2:
#         angles = hp.pix2ang(NSIDE, pixelnr,nest=False)
#         theta = angles[0]
#         phi = angles[1]
#         #print theta
#         #rotate point by desired angles:
#         x  = np.sin(phi0) * np.sin(theta) * np.cos(phi) - np.cos(theta0) * np.cos(phi0) * np.sin(theta) * np.sin(phi) + np.sin(theta0) * np.cos(phi0) * np.cos(theta) 
#         y  = np.cos(phi0) * np.sin(theta) * np.cos(phi) + np.cos(theta0) * np.sin(phi0) * np.sin(theta) * np.sin(phi) - np.sin(theta0) * np.sin(phi0) * np.cos(theta)
#         z  =                                              np.sin(theta0) *                np.sin(theta) * np.sin(phi) + np.cos(theta0) *                np.cos(theta) 
#         newpixelnr = hp.vec2pix(NSIDE,x ,y ,z , nest=False)
#         #newpixelnr = pixelnr
#         ROI.append(newpixelnr)
#         
#         pixelnr = pixelnr + 1
#         if pixelnr in [1,np.ceil(0.01*npix), np.ceil(0.05*npix),np.ceil(0.1*npix),np.ceil(0.2*npix),np.ceil(0.3*npix),np.ceil(0.4*npix),np.ceil(0.5*npix),np.ceil(0.6*npix),np.ceil(0.7*npix),np.ceil(0.8*npix),np.ceil(0.9*npix)]:
#             print int(pixelnr*100/npix), "% of total pixels scanned in", round((time.time() - start_time)/60,2), "minutes"
# 
# ROI=sorted(ROI)
# print ROI
##########################
# old algorithm:
# create region of interest: further computing only in the surrounding of the fov:
# fasten computation time: create ROI only in area phi: -phi0, +phi0, theta: -theta0. +theta0:

# for i in range(npix):
#     #print i
#     angles = hp.pix2ang(NSIDE, i,nest=False)
#     if np.arccos(np.sin(theta0-np.pi/2)*np.sin(angles[0]-np.pi/2) + np.cos(theta0-np.pi/2)*np.cos(angles[0]-np.pi/2)*np.cos(phi0-angles[1]))<ROIwidth/2:
#         roi.append(i)
#     if i in [1,np.ceil(0.01*npix), np.ceil(0.05*npix),np.ceil(0.1*npix),np.ceil(0.2*npix),np.ceil(0.3*npix),np.ceil(0.4*npix),np.ceil(0.5*npix),np.ceil(0.6*npix),np.ceil(0.7*npix),np.ceil(0.8*npix),np.ceil(0.9*npix)]:
#         print int(i*100/npix), "% done in", round((time.time() - start_time)/60,2), "minutes"
##########################
        
end_time = time.time()
print end_time - start_time,  "seconds"




filename = "anisotropyROI_nside"+str(NSIDE)+"_fov"+str(sigmaFOVdeg)+"degrees_ROIwidth"+str(ROIwidthfactor)+"test"
outputname = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"+"%s.dat" % filename 
##########################

         
outputfile = open(outputname, 'wb')
pickle.dump(ROI, outputfile)

# check cleverness of ROI-vector and plot : high-speed creation of a mask of region of interest:
npixROI = len(ROI)
print "length of ROI", npixROI
ROImask=np.zeros(npix)

for i in range(npixROI):
    ROImask[ROI[i]] = 1
hp.mollview(ROImask,nest=False)
pylab.title('Region of interest for resolution = %s$^\circ$, sigma_FOV = %s$^\circ$, ROI diameter = %s sigma$_{FOV}$'%(round(resolutiondeg,2),sigmaFOVdeg,ROIwidthfactor))
plt.show()# 


print "script finished"
