# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from textwrap import wrap
import shutil # for file copying

## give default variables:
# grid resolution:
NSIDE = 2**9
# gaussian width of fov in degs:
sigmaFOVdeg = 2.5
# center of fov on celestial sphere:
phi0deg =  0.0
theta0deg = 90.0
# ROI width:
ROIwidthfactor = 10
# gaussian width of point spread function:
sigmaPSFdeg = 0.1
# number of simulated events:
nevents = 10**4
# ratio signal events to isotropic background noise events:
# fsig = N_sig/(N_sig + N_back)
fsig = 0.0
# power spectrum index for simulated skymaps:
s = 2.0
# ratio DM induced events to astrophysical events:
fDM = 0.0
# runnumber:
runnumber = "testlocal5"
# output directory:
directory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"

# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:r:f:p:e:g:d:s:u:o:",["nside=","roi=","fov=","psf=","events=","fsig=","fDM=","s=","run=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -r <ROIwidthfactor> (Standard: 10)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -s <index s>,       (Standard: 0.0)'
    print ' -u <run index>      (Standard: "test")'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -r <ROIwidthfactor> (Standard: 10)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -s <index s>,       (Standard: 0.0)'
        print ' -u <run index>      (Standard: "test")'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-r", "--roi"):
        ROIwidthfactor = float(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-s", "--s"):
        s = float(arg)
    elif opt in ("-u", "--run"):
        runnumber = arg

# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)


## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)
# give celestial position in rads:
phi0 =  np.pi*phi0deg/180.0
phi0lat = -phi0 # healpy counts phi clockwise!!
theta0 = np.pi*theta0deg/180.0
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars


# give out input variables:
print  " "
print  "** INPUT PARAMETERS: **"
print "NSIDE: ",NSIDE
print "Number of grid pixels on whole sphere: ",npix
print "Using gaussian FOV acceptance..."
print "Full diameter of FOV acceptance (1 sigma): ",sigmaFOVdeg,"°"
print "Full diameter of gaussian PSF (1 sigma): ",sigmaPSFdeg,"°" 
print "Resolution of skymap in degs: ",resolutiondeg, "°"
print "Full diameter of ROI: ",ROIwidthfactor, "*",sigmaFOVdeg,"°"
print "Number of MC events: ",nevents
print "Ratio fsig: ",fsig
print "Ratio fDM: ",fDM
print "Power spectrum index s: ",s
print "Maximum resolvable multipole index l (lmax): ",lmax
print "** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS ADD FLAG -h AT STARTING **"
print  " "

start_time = time.time()

# load regions of interest created by createroi.py: further computing only in the surrounding of the fov
# to fasten the MC simulation computing time. Attention: This influences the final power spectrum, The smaller
# the ROI region, the bigger the artefacts of limiting the computation to the ROI. The size of the ROI is 
# determined in the createroi.py script as a multiple of the sigmaFOV (standard: diameter of ROI is 20 times
# sigmaFOV).
print "Load ROI file..."
filename = "anisotropyROI_nside"+str(NSIDE)+"_fov"+str(sigmaFOVdeg)+"degrees_ROIwidth"+str(ROIwidthfactor)
inputname = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/"+"%s.dat" % filename 
inputfile = open(inputname, 'r')
ROI = pickle.load(inputfile)
npixROI = len(ROI) # number of grid pixels in ROI
print "Number of grid pixels in ROI:", npixROI



outputpath = str(directory)+"nside"+str(NSIDE)+"_FOV"+str(sigmaFOVdeg)+"degs"+"_PSF"+str(sigmaPSFdeg)+"degs"+"_fsig"+str(fsig)+"_fDM"+str(fDM)+"_s"+str(s)+"_"+str(nevents)+"events_ROIwidth"+str(ROIwidthfactor)+"_run"+str(runnumber)

if not os.path.exists(outputpath): os.makedirs(outputpath)

# copy script into output directory:
scriptdirectory = os.getcwd()
shutil.copy2(str(scriptdirectory)+"/anisotropyToyMC.py", outputpath)

print "Write output files to directory", outputpath

# load output files:
filename = "eventmap"
outputname_eventmap = str(outputpath)+"/%s.dat" % filename 
outputfile_eventmap = open(outputname_eventmap, 'wb')
filename = "plots"
outputname_plots = str(outputpath)+"/%s.pdf" % filename 
outputfile_plots = pdf(outputname_plots)
filename = "clspectrum"
outputname_clspectrum = str(outputpath)+"/%s.dat" % filename 
outputfile_clspectrum = open(outputname_clspectrum, 'wb')

# create neccessary fields:
print "Create neccessary fields..."
skymap = np.zeros(npix) # simulated probability skymap with specific power spectrum to simulate probability of signal events
FOVmask = np.zeros(npixROI) # gaussian weighted mask of field of view
eventmap = np.zeros(npix) # simulated event skymap  containing background and signal events
print "Fields generated after", round((time.time() - start_time)/60,2), "minutes."
clIN = np.zeros(lmax) # vector of c(l)'s


## generate skymap:
print " "
print "Fill skymap..."
# generate cl's:
for l in range(1,len(clIN)):
    clIN[l] = clnorm*l**s/(l*(l+1))
# perform multipole transformation to get probability skymap:
skymap = hp.synfast(clIN, NSIDE)
# normalize probability skymap:
skymap = (skymap-min(skymap))/(max(skymap)-min(skymap))
print "Skymap filled after", round((time.time() - start_time)/60,2), "minutes. Saving plot and reduce skymap to ROI..."
# # plot probability skymap
plot_skymap = plt.figure(figsize=(9,6.1))
plt.axis('off')
title = 'Normalized signal probability skymap for s = %s, resolution = %s$^\circ$'%(s,round(resolution,2))
plt.title('\n'.join(wrap(title,90)))
plt.title('\n'.join(wrap(title,90)))
hp.mollview(skymap,nest=False, fig=1, title='', unit='event detection probability')
hp.graticule(dpar=5, dmer=5)
outputfile_plots.savefig(plot_skymap)
# downsize skymap to ROI:
skymapRED = np.zeros(npixROI) # skymap reduced to ROI
for ROIpixelnr in range(npixROI):
    skymapRED[ROIpixelnr] = skymap[ROI[ROIpixelnr]]
del skymap


## generate gaussian mask of field of view:
print " "
print "Generate gaussian mask of field of view..."
for ROIpixelnr in range(npixROI):
    pixelnr = ROI[ROIpixelnr]
    angles = hp.pix2ang(NSIDE, pixelnr,nest=False)
    thetaFOV  = angles[0]
    phiFOV = -angles[1]
    # here, the shape of the acceptance is defined:
    FOVmask[ROIpixelnr] = np.e**(-0.5*(np.arccos(np.sin(theta0-np.pi/2)*np.sin(thetaFOV-np.pi/2) + np.cos(theta0-np.pi/2)*np.cos(thetaFOV-np.pi/2)*np.cos(phi0-phiFOV)))**2/(sigmaFOV)**2)
print "FOVmask filled after", round((time.time() - start_time)/60,2), "minutes." 
## creation of eventmap:
# 1. step:  it is chosen, if a random or a signal event takes place:
# Again, a uniform deviate between 0 and 1 is generated and compared with fsig.
# If randomnr1>fsig: It is a background event! -> Go to background event creation procedure
# If randomnr1<fsig: It is a signal event! -> Go to signal event creation procedure.
# 
# 2. step: selection of pixel, where an event takes place: 
#  - in the noise case, at first, an arbitrary pixel within the ROI is chosen 
# (with a second random number, randompixelnr). Then the Gaussian function
# at this pixel is evaluated and compared with a random number 3, uniformly distributed
# between 0 and 1. If this random number 3 is smaller than the Gaussian value, then an event is \
# added to that pixel.
# - in the signal case: the probability skymap value at that point is compared with a random
#   number 4: If randomnr3<probskymap: An event is added. If not: go back to step 2 (for a signal event).

# The procedure will be repeated until n events are counted (eventnumber=nevents).

# and now the code:
 
eventnumber = 0 # to count events from zero to nevents
backgroundcounts = 0 # to later check ratio between signal and background events.
signalcounts = 0  # to later check ratio between signal and background events.

print " "
print "Now start Monte Carlo: Fill eventmap with events..."
# now repeat above procedure until n events are counted:
# count computation time

while eventnumber<nevents:
    eventbool = 0
    # first choose if signal or background event:
    randomnumber1 = rn.randint(0, 10*nevents)/(10.0*nevents)
    if randomnumber1>fsig:
            # it is a background event, now select arbitrary pixel within the ROI:
            randomROIpixelnr = rn.randint(0, npixROI-1)
            randompixelnr = ROI[randomROIpixelnr] # will be needed to fill eventmap
            # the event has not yet detected, still has to pass acceptance function of fov:
            randomnumber3 = rn.randint(0, 10*nevents)/(10.0*nevents)
            if randomnumber3<FOVmask[randomROIpixelnr]:
                # o.k., has succesfully passed the acceptance window, now it really is detected.
                backgroundcounts = backgroundcounts + 1
                eventbool = 1                 
                #info = " background!"
    else:
            # It's a signal event! Now, don't select pixel uniformly distributed within the ROI,
            # but according to the probability skymap:
            signalbool = 0
            while signalbool==0:
                randomROIpixelnr = rn.randint(0, npixROI-1)
                randompixelnr = ROI[randomROIpixelnr] # willl be needed to fill eventmap
                randomnumber4 = rn.randint(0, 10*nevents)/(10.0*nevents)
                if randomnumber4<skymapRED[randomROIpixelnr]:
                    signalbool = 1
                    # great, at point [pixelnr] really a signal event takes place, but again has to 
                    # pass the acceptance function:
                    randomnumber3 = rn.randint(0, 10*nevents)/(10.0*nevents)
                    if randomnumber3<FOVmask[randomROIpixelnr]:
                        # o.k., has succesfully passed the acceptance window, now it really is detected!
                        signalcounts = signalcounts + 1
                        eventbool = 1
                        #info = " signal!"
    # now finally count event:
    if eventbool==1:
            ## count event at pixel "randompixelnr", but still displace by PSF:
            # get angular position of event:
            angles = hp.pix2ang(NSIDE, randompixelnr,nest=False)
            thetaEVENT  = angles[0]
            phiEVENT = -angles[1]
            #get radial coordinate of displacement by a gaussian distribution:
            gauss1 = rn.gauss(0, sigmaPSF)
            gauss2 = rn.gauss(0, sigmaPSF)
            #get angular distance of displacement:
            d = np.sqrt(gauss1**2 + gauss2**2)
            # correct sign lost by arcustangens:
            if gauss1<0: 
                philocal =  np.arctan(gauss2/gauss1)+np.pi
            else:
                philocal =  np.arctan(gauss2/gauss1)
            # now apply rotation matrix to displace event by distance d, angle philocal not from the north pole, but from theta_event, phi_event
            x  = np.sin(phiEVENT) * np.sin(d) * np.cos(philocal) - np.cos(thetaEVENT) * np.cos(phiEVENT) * np.sin(d) * np.sin(philocal) + np.sin(thetaEVENT) * np.cos(phiEVENT) * np.cos(d) 
            y  = np.cos(phiEVENT) * np.sin(d) * np.cos(philocal) + np.cos(thetaEVENT) * np.sin(phiEVENT) * np.sin(d) * np.sin(philocal) - np.sin(thetaEVENT) * np.sin(phiEVENT) * np.cos(d)
            z  =                                                   np.sin(thetaEVENT) *                    np.sin(d) * np.sin(philocal) + np.cos(thetaEVENT) *                    np.cos(d) 
            randompixelnr = hp.vec2pix(NSIDE,x ,y ,z , nest=False)
         
            eventmap[randompixelnr] = eventmap[randompixelnr] + 1
            eventnumber = eventnumber + 1
            # print out computation times:
            if eventnumber in [0.1*nevents, 0.2*nevents, 0.3*nevents, 0.4*nevents, 0.5*nevents, 0.6*nevents, 0.7*nevents, 0.8*nevents, 0.9*nevents]:
                print int(eventnumber*100/nevents), "% done after", round((time.time() - start_time)/60,2), "minutes."



del FOVmask             

print "Eventmap filled with total", nevents, "events." 
print "Signal events: ", signalcounts
print "Background events: ", backgroundcounts
print " "
# smoothe eventmap with PSF:
#print "Smoothing eventmap within PSF..."
#eventmap = hp.smoothing(eventmap, fwhm=sigmaPSF)

print "Saving eventmap within ROI to file..."
# reduce eventmap to ROI:
eventmapRED = np.zeros(npixROI)
for ROIpixelnr in range(npixROI):
    eventmapRED[ROIpixelnr] = eventmap[ROI[ROIpixelnr]]

# write output file:        
pickle.dump(eventmapRED, outputfile_eventmap)
outputfile_eventmap.close()
del eventmapRED

print "Saving plots of eventmap..."
# plot event map:
plot_eventmap = plt.figure(figsize=(9,6.1))
plt.axis('off')
title = 'Event skymap for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s$^\circ$, PSF = %s$^\circ$, DM ratio = %s, s = %s, resolution = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,s,round(resolutiondeg,2))
plt.title('\n'.join(wrap(title,90)))
#plt.subplots_adjust(top=0.95)  
hp.mollview(eventmap,nest=False, fig=2, unit='events per pixel', title='') 
hp.graticule(dpar=5, dmer=5)
outputfile_plots.savefig(plot_eventmap)
# plot 2d/profile:
phimax = 5 * sigmaFOV # in deg
phipixelnr = 2*int(phimax/resolution)+1 # always an odd number
deltaphi = 2*phimax/(phipixelnr-1) # in deg
phi = np.zeros(phipixelnr)
profile = np.zeros(phipixelnr)
pixelnr = np.zeros(phipixelnr)
gausscurve = np.zeros(phipixelnr)
for i in range(phipixelnr):
    phi[i] = -phimax + i * deltaphi
    pixelnr[i] = hp.ang2pix(NSIDE, theta0, phi0lat +phi[i],nest=False)
    profile[i] = eventmap[pixelnr[i]]
for i in range((phipixelnr-1)/2):
    phi[i] = i * deltaphi
    gausscurve[(phipixelnr-1)/2-i] = max(profile)*np.exp(-0.5*phi[i]**2/(sigmaFOV )**2) 
    gausscurve[i+(phipixelnr-1)/2] = max(profile)*np.exp(-0.5*phi[i]**2/(sigmaFOV )**2)
    phi[i] = -phimax + i * deltaphi
deltaphideg = deltaphi*180/np.pi
phideg = 180*phi/np.pi      
plot_eventmapprofile = plt.figure(figsize=(9, 7))
plt.bar(phideg, profile , width=deltaphideg,  facecolor='g')    
plt.plot(phideg,gausscurve,'b',linewidth=1.5)
plt.xlabel(' Delta phi in degrees (phi =  phi_0 +- Delta phi)')
plt.ylabel(' event counts per pixel')
title ='Profile of  event skymap at  theta theta_0  for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s$^\circ$, PSF = %s$^\circ$, DM ratio = %s, s = %s, resolution = %s$^\circ$, # of slots: %s, delta phi = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,s,round(resolutiondeg,2),phipixelnr,round(deltaphideg,2))
plt.title('\n'.join(wrap(title,90)))
plt.subplots_adjust(top=0.87) 
outputfile_plots.savefig(plot_eventmapprofile)
plt.close()
  
#### evaluate power spectrum:
print ""
print "Evaluate power spectrum of eventmap..."
# normalize eventmap:
eventmap = -1.0 + eventmap*npix/nevents
 
# compute power spectrum:

clSIM = np.zeros(lmax+1) # simulated "pure" power spectrum without noise
clSIMnoise = np.zeros(lmax+1) # simulated power spectrum of noise
LMAX = lmax

clOUT = hp.anafast(eventmap, lmax=LMAX)

del eventmap
## re-compute input power spectrum with same normalization Integral_wholesphere = 4Pi\
## for comparison with output power spectrum:
# re-generate cl's:
for l in range(1,lmax+2):
    clSIM[l-1] =  1.0*l**s/(l*(l+1)) # the norm clnorm is not considered here because the spectrum
#                                     # is renormalized anyway.
    clSIMnoise[l-1] =  10**(-5)*l**2/(l*(l+1)) # the norm clnorm is not considered here because the spectrum
#                                     # is renormalized anyway.
# perform multipole transformation to get probability skymap:
#skymap = hp.synfast(clsim, NSIDE)
#skymap = (skymap-min(skymap))
# sum all events in skymap and renormalize:
#skymapevents = 0
#for i in range(npix):
#    skymapevents = skymapevents + skymap[i]
##skymap = skymap*npix/skymapevents
#skymap = skymap/skymapevents*npix
#
##transform back into power spectrum:
#clsim = hp.anafast(skymap, lmax=LMAX)

# save spectrum to file:
pickle.dump(clOUT, outputfile_clspectrum)
outputfile_clspectrum.close()


# plot power spectrum:
plot_powerspectrum = plt.figure(figsize=(9, 6))
ell = np.arange(len(clOUT))

#plt.plot(ell, (ell) * (ell+1) * clsim/(2*np.pi),color='b')# 
plt.plot(ell, ell * (ell+1) * clOUT/(2*np.pi),color='r')#
plt.plot(ell, ell * (ell+1) * clSIM/(2*np.pi),color='k')#
plt.plot(ell, ell * (ell+1) * clSIMnoise/(2*np.pi),'k--')#
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
plt.xscale('log')
plt.yscale('log')
pylab.xlim([1,lmax])
pylab.ylim([10**(-4),10**6])
title = 'Angular power spectrum from normalized event skymap for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s$^\circ$, DM ratio = %s, s = %s, resolution = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,fDM,s,round(resolution,2))
plt.title('\n'.join(wrap(title,90)))
plt.subplots_adjust(top=0.85)
#plt.fill_between(ell, ell * (ell+1) * cl/(2*np.pi),ell * (ell+1) * cl2/(2*np.pi),color='r',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * cl/(2*np.pi),ell * (ell+1) * cl3/(2*np.pi),color='r',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * cl2/(2*np.pi),ell * (ell+1) * cl3/(2*np.pi),color='r',alpha=1)
 
outputfile_plots.savefig(plot_powerspectrum)

outputfile_plots.close()

print "** Script anisotropyToyMC.py finished after",  round((time.time() - start_time)/60,2), "minutes. **"
print ""

## plots:

# check cleverness of roi-vector and plot : high-speed creation of a mask of region of interest:
#for i in range(npixROI):
#     roimask[roi[i]] = 1
#hp.mollview(roimask,nest=False)
#pylab.title('Region of interest for resolution = %s$^\circ$, sigma_FOV = %s$^\circ$, ROI diameter = %s sigma$_{FOV}$'%(round(resolution,2),sigmaFOVdeg,ROIwidthfactor))
# 
# # plot field of view mask:
# hp.mollview(FOVmask,nest=False)
# pylab.title('Gaussian acceptance of FOV = %s$^\circ$ (resolution = %s$^\circ$)'%(sigmaFOVdeg,round(resolution,2)))
# 
# # plot input spectrum:
# ell = np.arange(len(cl))
# plt.figure()
# plt.plot(ell, ell * (ell+1) * cl/(2*np.pi))
# plt.xlabel('multipole index l'); plt.ylabel('l(l+1)$c_l$/2$\pi$'); plt.grid()
# plt.xscale('log')
# plt.yscale('log')
# pylab.title('Input power spectrum for s = %s, DM ratio = %s,\n resolution = %s$^\circ$ (determining $l_{max}$)'%(s,fDM,round(resolution,2)))
# 
