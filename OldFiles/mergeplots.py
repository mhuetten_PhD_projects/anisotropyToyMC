# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from textwrap import wrap
import shutil # for file copying

## give default variables:
# grid resolution:
NSIDE = 2**12
# gaussian width of fov in degs:
sigmaFOVdeg = 2.5
# ROI width:
ROIwidthfactor = 10
# gaussian width of point spread function:
sigmaPSFdeg = 0.1
# number of simulated events:
nevents = 10**7
# ratio signal events to isotropic background noise events:
# fsig = N_sig/(N_sig + N_back)
fsig = 1.0
# ratio DM induced events to astrophysical events:
fDM = 0.0
# runnumber:
runnumber = "01"
# output directory:
directory = "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/old_data/"

# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:r:f:p:e:g:d:s:u:o:",["nside=","roi=","fov=","psf=","events=","fsig=","fDM=","s=","run=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -r <ROIwidthfactor> (Standard: 10)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -s <index s>,       (Standard: 0.0)'
    print ' -u <run index>      (Standard: "test")'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -r <ROIwidthfactor> (Standard: 10)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -s <index s>,       (Standard: 0.0)'
        print ' -u <run index>      (Standard: "test")'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-r", "--roi"):
        ROIwidthfactor = float(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-s", "--s"):
        s = float(arg)
    elif opt in ("-u", "--run"):
        runnumber = arg

# Power constant normalization for Blazars and DM:
Clblazars = 10**(-5)
ClDM = 10**(-3)


## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)
# determine maximum calculable cl from grid resolution:
lmax = int(np.pi/hp.nside2resol(NSIDE))
# calculate clnorm from ratio Dm induced events to astrophysical events:
clnorm = fDM*ClDM + (1-fDM)*Clblazars

s=2.0
runnumber=[0.0,0.5,1.5,2.0,2.5]
clspectrum = np.ndarray((len(runnumber), lmax+1))


print "Load power spectrum files..."

filename = "clspectrum"

for i in range(len(runnumber)):
    if runnumber[i]  == 0.0:
        inputpath = str(directory)+"nside"+str(NSIDE)+"_FOV"+str(sigmaFOVdeg)+"degs"+"_PSF"+str(sigmaPSFdeg)+"degs"+"_fsig"+str(0.0)+"_fDM"+str(fDM)+"_s"+str(runnumber[i])+"_"+str(nevents)+"events_ROIwidth"+str(ROIwidthfactor)+"_run01"
    else:
        inputpath = str(directory)+"nside"+str(NSIDE)+"_FOV"+str(sigmaFOVdeg)+"degs"+"_PSF"+str(sigmaPSFdeg)+"degs"+"_fsig"+str(fsig)+"_fDM"+str(fDM)+"_s"+str(runnumber[i])+"_"+str(nevents)+"events_ROIwidth"+str(ROIwidthfactor)+"_run01"
    inputname_clspectrum = str(inputpath)+"/%s.dat" % filename 
    inputfile = open(inputname_clspectrum, 'r')
    inputdata = pickle.load(inputfile)
    print lmax, len(inputdata)
    for j in range(len(inputdata)):
        clspectrum[i][j] = inputdata[j]





  
#### compare power spectra:
 
# compute power spectrum:
 
clSIM = np.zeros(lmax+1) # simulated "pure" power spectrum without noise
clSIMnoise = np.zeros(lmax+1) # simulated power spectrum of noise
LMAX = lmax
 
 
## re-compute input power spectrum with same normalization Integral_wholesphere = 4Pi\
## for comparison with output power spectrum:
# re-generate cl's:
for l in range(1,lmax+2):
#    clSIM[l-1] =  1.0*l**s/(l*(l+1)) # the norm clnorm is not considered here because the spectrum
#                                     # is renormalized anyway.
    clSIMnoise[l-1] =  10**(-5)*l**2/(l*(l+1)) # the norm clnorm is not considered here because the spectrum
#                                     # is renormalized anyway.

 
# save spectrum to file:
# pickle.dump(clOUT, outputfile_clspectrum)
# outputfile_clspectrum.close()
 
 
# plot power spectrum:
plot_powerspectrum = plt.figure(figsize=(9, 6))
ell = np.arange(lmax+1)
 
#plt.plot(ell, (ell) * (ell+1) * clsim/(2*np.pi),color='b')# 
for i in range(len(runnumber)):
    plt.plot(ell, ell * (ell+1) * clspectrum[i]/(2*np.pi))#
#plt.plot(ell, ell * (ell+1) * clSIM/(2*np.pi),color='k')#
plt.plot(ell, ell * (ell+1) * clSIMnoise/(2*np.pi),'k--')#
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
plt.xscale('log')
plt.yscale('log')
pylab.xlim([1,lmax])
pylab.ylim([10**(-4),10**6])
title = 'Angular power spectrum from normalized event skymap for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s$^\circ$, DM ratio = %s, s = %s, resolution = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,fDM,s,round(resolution,2))
plt.title('\n'.join(wrap(title,90)))
plt.subplots_adjust(top=0.85)
plt.show()
#plt.fill_between(ell, ell * (ell+1) * cl/(2*np.pi),ell * (ell+1) * cl2/(2*np.pi),color='r',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * cl/(2*np.pi),ell * (ell+1) * cl3/(2*np.pi),color='r',alpha=1)
#plt.fill_between(ell, ell * (ell+1) * cl2/(2*np.pi),ell * (ell+1) * cl3/(2*np.pi),color='r',alpha=1)
#  
# outputfile_plots.savefig(plot_powerspectrum)
# 
# outputfile_plots.close()