#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
   echo
   echo "Evaluation script for  Clumpy MonteCarlo Simulation:"
   echo
   echo "clumpyMC.plots.sh <outputdirectory> " 
   echo
   echo
   echo "  parameter should contain parameter numbers only"
   echo "  example for parameter list:"
   echo "    0.0"
   echo "    0.2"
   echo "    0.5"
   echo

   exit
fi

#read variables from parameter file:

runname=$1





# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"


WORKDIR=/lustre/fs5/group/cta/users/mhuetten/workdata/anisotropyMC

parameterfile=${WORKDIR}/${runname}/Inputfiles/clumpyMC.parameters

#read variables from parameter file:

# General parameters:
export runname=$(cat $parameterfile 	| head -n10 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $parameterfile 		| head -n13 | tail -n1 | sed -e 's/^[ \t]*//')
export inputfile=$(cat $parameterfile 	| head -n16 | tail -n1 | sed -e 's/^[ \t]*//')
export NSIDE=$(cat $parameterfile 		| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated telescope properties:
export sigmaFOVdeg=$(cat $parameterfile | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')
export psi0deg=$(cat $parameterfile 	| head -n28 | tail -n1 | sed -e 's/^[ \t]*//')
export theta0deg=$(cat $parameterfile 	| head -n32 | tail -n1 | sed -e 's/^[ \t]*//')
export sigmaPSFdeg=$(cat $parameterfile | head -n35 | tail -n1 | sed -e 's/^[ \t]*//')

# Simulated physics:
export nevents=$(cat $parameterfile 	| head -n41 | tail -n1 | sed -e 's/^[ \t]*//')
export fsig=$(cat $parameterfile 		| head -n44 | tail -n1 | sed -e 's/^[ \t]*//')
export fDM=$(cat $parameterfile 		| head -n47 | tail -n1 | sed -e 's/^[ \t]*//')

# If a run parameter file exists:

if [ -e "${outdir}/${runname}/Inputfiles/clumpyMC.runparameter" ]
then
	echo "evaluate multi run simulation"
	PLIST=${outdir}/${runname}/Inputfiles/clumpyMC.runparameter
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter

for AFIL in $PARAMETERS
do

   echo "now evaluating parameter run $runparameter = $AFIL"

   case $runparameter in
	"nevents") nevents=$AFIL;;
	"fsig") fsig=$AFIL;;
	"fDM") fDM=$AFIL;;
   esac

# rename $runname into:	
subrunname=$runname/$runparameter-$AFIL
		
python $SCRIPTDIR/clumpyMC.plots.py -n $NSIDE -l $psi0deg -t $theta0deg -v 30.0 -w 30.0 -f $sigmaFOVdeg -i $inputfile -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM  -o ${outdir}/${subrunname} #>> Plot.log

done
fi


if [ ! -e "${outdir}/${runname}/Inputfiles/clumpyMC.runparameter" ]
then

echo " evaluating single run"
  
python $SCRIPTDIR/clumpyMC.plots.py -n $NSIDE -l $psi0deg -t $theta0deg -v 30.0 -w 30.0 -f $sigmaFOVdeg -i $inputfile -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM  -o ${outdir}/${runname} #>> Plot.log
 
#rm $outdir/$runnumber/${AFIL}_eventmap.fits
#rm $outdir/$runnumber/${AFIL}_skymap.fits

fi

exit

