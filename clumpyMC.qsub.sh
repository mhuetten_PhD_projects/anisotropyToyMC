#$ -S /bin/tcsh
#
# script to analyse VTS raw files (VBF) with eventdisplay
#
# Author: Gernot Maier
#

#####################################
# parameters set by parent script
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DDDDD
set NSIDE=NSSSE
set sigmaFOVdeg=SSSSS
set psizerodeg=PPPPP
set thetazerodeg=TTTTT
set inputfile=ROOOI
set sigmaPSFdeg=PSSSF
set nevents=NNNNN
set fDM=FFFFF
set fsig=SIIIG


mkdir -p $ODIR
#mkdir -p $LDIR
echo
echo " **************************************"
echo " This is fancy ToyMonteCarlo Simulation"
echo " **************************************"
echo 
echo " *** INPUT PARAMETERS: ***"
echo 
echo -n " NSIDE: " $NSIDE"\n"
#echo -n "Number of grid pixels on whole sphere: " npix
echo -n " Theta coordinate of FOV center in standard spherical coordinates: " $thetazerodeg "degrees\n"
echo -n " Psi coordinate of FOV center in standard spherical coordinates: " $psizerodeg "degrees\n"
echo -n " Using gaussian FOV acceptance...\n"
echo -n " Full diameter of FOV acceptance (1 sigma): " $sigmaFOVdeg "degrees\n"
echo -n " Full diameter of gaussian PSF (1 sigma): " $sigmaPSFdeg "degrees\n"
#echo -n "Resolution of skymap in degs: ",resolutiondeg, "°"
echo -n " Clumpy input file: " $inputfile "\n"
echo -n " Number of MC events: " $nevents "\n"
echo -n " Ratio fsig: " $fsig "\n"
echo -n " Ratio fDM: " $fDM "\n"
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE clumpyMC.sub.sh ***"
echo 

#####################################
# set the right observatory (environmental variables)
#source $EVNDISPSYS/setObservatory.tcsh VERITAS
source /afs/ifh.de/user/m/mhuetten/setbatchenvironment.tcsh VERITAS
echo


echo " *** Create eventmap: ***"
$SCRIPTDIR/clumpyMC.montecarlo.py -n $NSIDE -f $sigmaFOVdeg  -p $sigmaPSFdeg -l $psizerodeg -t $thetazerodeg  -e $nevents -g $fsig -d $fDM -i $inputfile -o $ODIR 
echo

echo " *** Evaluate Output spectrum with Anafast routine: ***"
cd $HEALPIX #/afs/ifh.de/group/cta/scratch/mhuetten/Programs/healpix
$HEALPIX/binfortran/anafast -s $ODIR/anafast.par
 
cd $ODIR
rm eventmap.fits
echo
echo " *** clumpyMC successfully finished. ***"
echo
##sleep 20

##exit
