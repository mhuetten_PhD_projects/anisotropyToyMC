#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

###############################################################################
# Changelog:

#
###############################################################################

##    import modules:    ######################################################
###############################################################################

#import MoritzSphericalTools
import numpy as np
import os
import sys
import getopt
import os
import sys
import getopt
import pylab
from matplotlib import pyplot as plt
import healpy as hp
import MoritzSphericalTools

##    define local functions:    ##############################################
###############################################################################






##    main part:   ############################################################
###############################################################################

def main(argv):
    
    ###########################################################################
    # read input variables
    
    NSIDE = 512
    s = 2.
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hn:f:p:s:o:",["nside=","fov=","psf=","s=","dirout="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -s <index s>,       (Standard: 0.0)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'The input options are:'
            print ' -n <NSIDE>          (Standard: 2^9)'
            print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
            print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
            print ' -s <index s>,       (Standard: 0.0)'
            print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
            sys.exit()
        elif opt in ("-n", "--nside"):
            NSIDE = int(arg)
        elif opt in ("-f", "--fov"):
            sigmaFOVdeg = float(arg)
        elif opt in ("-p", "--psf"):
            sigmaPSFdeg = float(arg)
        elif opt in ("-s", "--s"):
            s = float(arg) 
        elif opt in ("-o", "--dirout"):
            outputdirectory = arg
    
    print " initializing script toyMC.checkFolding.py"

    LMAX = int(np.pi/hp.nside2resol(NSIDE))
    LMAX1 = 3*NSIDE - 1
    print LMAX,LMAX1
    ClDM = 10**(-3)
    clnorm = ClDM
    outputdirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/deconvolve-test/"

    ###########################################################################
    # generate artficial skymap:
    
    # generate cl's:
    clIN = np.zeros(LMAX) # vector of c(l)'s
    for l in range(1,len(clIN)):
        clIN[l] = clnorm*l**s/(l*(l+1))
    print "clIN",len(clIN)
        
    resolution = hp.nside2resol(NSIDE) * 2.0 / np.sqrt(np.pi)
    print resolution
    
    # generate skymap:
    
    fullskymap = hp.synfast(clIN, NSIDE, lmax=LMAX, pixwin=False, sigma=resolution/20)
     
    fullskymap = (fullskymap-min(fullskymap))/(max(fullskymap)-min(fullskymap))
     
    # generate masked skymap:
#     npix = hp.nside2npix(NSIDE)
#     maskedskymapWindow = np.zeros(npix)-1.6375*10**(30)
#     maskedskymapGauss = np.zeros(npix)-1.6375*10**(30)
#     maskWindow = np.zeros(npix)-1.6375*10**(30)
#     maskGauss = np.zeros(npix)-1.6375*10**(30)
#           
#     thetaZero = 60./180.*np.pi
#     psiZero = 30./180.*np.pi
#     theta_cutoff_mask = 5./180.*np.pi
#     sigma_mask = theta_cutoff_mask/3.
#           
#     status = [10,20,30,40,50,60,70,80,90,100]
#     nstatus = 0
#     for i in range(npix):
#         #print int(float(i)/float(npix)*100)      
#         if int(float(i)/float(npix)*100)==status[nstatus]:
#             print status[nstatus],"%"
#             nstatus = nstatus + 1
#         [theta, psi] = hp.pix2ang(NSIDE,i)
#         angdist = MoritzSphericalTools.angularDistance(thetaZero, psiZero, theta, psi)
#         if angdist < theta_cutoff_mask:
#             maskWindow[i] = 1.
#         else:
#             maskWindow[i] = 0.
#         maskGauss[i] = np.exp(-(angdist**2/(2*sigma_mask**2)))
#         maskedskymapWindow[i] = fullskymap[i] * maskWindow[i]
#         maskedskymapGauss[i] = fullskymap[i] * maskGauss[i]
#     
    fullskymap = hp.read_map(outputdirectory+"fullskymap.fits")
    maskedskymap = hp.read_map(outputdirectory+"maskedskymap.fits")
    
    maskGauss = hp.read_map(outputdirectory+"maskGauss.fits")
    maskWindow = hp.read_map(outputdirectory+"maskWindow.fits")
    
    maskedskymapGauss = hp.read_map(outputdirectory+"maskedskymapGauss.fits")
    maskedskymapWindow = hp.read_map(outputdirectory+"maskedskymapWindow.fits")
    
    #hp.mollview(fullskymap, title="Mollview image RING")

    #hp.mollview(maskedskymapGauss, title="Mollview image RING")
    #hp.mollview(maskGauss, title="Mollview image RING")
    
#     hp.write_map(outputdirectory+"fullskymap.fits",fullskymap)
#     hp.write_map(outputdirectory+"maskedskymapGauss.fits",maskedskymapGauss)
#     hp.write_map(outputdirectory+"maskedskymapWindow.fits",maskedskymapWindow)
#     hp.write_map(outputdirectory+"maskGauss.fits",maskGauss)
#     hp.write_map(outputdirectory+"maskWindow.fits",maskWindow)


    clOut_fullsky = hp.anafast(fullskymap, map2=None, nspec=None, lmax=LMAX-1, iter=0, alm=False, pol=True, use_weights=False, regression=True, datapath=None)
    
    #clOut_cutsky = hp.anafast(maskedskymapGauss, map2=None, nspec=None, lmax=LMAX-1, iter=0, alm=False, pol=True, use_weights=False, regression=True, datapath=None)    
    clOut_cutsky = hp.anafast(maskedskymapWindow, map2=None, nspec=None, lmax=LMAX-1, iter=0, alm=False, pol=True, use_weights=False, regression=True, datapath=None)
    
    clOut_maskGauss = hp.anafast(maskGauss, map2=None, nspec=None, lmax=LMAX-1, iter=0, alm=False, pol=True, use_weights=False, regression=True, datapath=None)
    clOut_maskWindow = hp.anafast(maskWindow, map2=None, nspec=None, lmax=LMAX-1, iter=0, alm=False, pol=True, use_weights=False, regression=True, datapath=None)


#         
#     maskGaussfilepath=outputdirectory+"cl_of_maskGauss.txt"
#     np.savetxt(maskGaussfilepath, clOut_maskGauss)
    
    #clOut_fullsky_spice = np.loadtxt(outputdirectory+"cl_fullsky_spice.txt")
    cl_pseudo_cutskyGauss_spice = np.loadtxt(outputdirectory+"cl_pseudo_cutskyGauss_spice.txt")
    cl_pseudo_cutskyWindow_spice = np.loadtxt(outputdirectory+"cl_pseudo_cutskyWindow_spice.txt")
    
    cl_of_maskGauss_spice = np.loadtxt(outputdirectory+"cl_of_maskGauss_spice.txt")
    
    cl_of_maskGauss_spice_vector = np.zeros(1536)
    
    for i in range(len(cl_of_maskGauss_spice_vector)):
        cl_of_maskGauss_spice_vector[i] = cl_of_maskGauss_spice[i,1]
    
    cl_of_maskWindow_spice = np.loadtxt(outputdirectory+"cl_of_maskWindow_spice.txt")

    cl_deconvolved_cutskyGauss_spice = np.loadtxt(outputdirectory+"cl_deconvolved_cutskyGauss_spice.txt")
    cl_deconvolved_cutskyWindow_spice = np.loadtxt(outputdirectory+"cl_deconvolved_cutskyWindow_spice.txt")
    
    
    
    
    #clOut_deconvolved_spice = np.loadtxt(outputdirectory+"cl_cutsky_spice.txt") 
    
    
    cutskyfilepath=outputdirectory+"cl_cutsky.txt"
    maskfilepath=outputdirectory+"cl_of_mask.txt"
    np.savetxt(cutskyfilepath, clOut_cutsky)
    np.savetxt(maskfilepath, clOut_maskWindow)
    #np.savetxt(maskfilepath, cl_of_maskGauss_spice_vector)
    
    binsize = 50
    clOut_deconvolved = MoritzSphericalTools.deconvolve_mask(len(clOut_maskWindow), cutskyfilepath, maskfilepath, binsize)
#     
#     
# 
#     print "outputarray:",clOut_deconvolved[:,0],clOut_deconvolved[:,0] * (clOut_deconvolved[:,0]+1) * clOut_deconvolved[:,1]/(2*np.pi)
#     
#     clOut_deconvolvedlength = len(clOut_deconvolved[:,0])
#     
#     outputarray1 = np.zeros((clOut_deconvolvedlength,2))
#     
#     for i in range(clOut_deconvolvedlength):
#         outputarray1[i,0] = clOut_deconvolved[i,0]
#         outputarray1[i,1] = clOut_deconvolved[i,1]
#         if outputarray1[i,1]<0:
#             median0 = (outputarray1[i-1,0] + outputarray1[i,0])/2.
#             median1 = (outputarray1[i-1,1] + outputarray1[i,1])/2.
#             outputarray1[i-1,1] = median1
#             outputarray1[i,1] = median1
#             outputarray1[i-1,0] = median0
#             outputarray1[i,0] = median0
#     #data2 = np.loadtxt(outputpath_multipole_deconvolved_cl)
#     
#     #print "outputarray1:",outputarray1[:,0],outputarray1[:,0] * (outputarray1[:,0]+1) * outputarray1[:,1]/(2*np.pi)
#     
#     #print data2
#     print "len(clOut_fullsky):",len(clOut_fullsky)
#     print "outputarraylength:",clOut_deconvolvedlength
    
    ellIN = np.arange(len(clIN))
   
    plot_powerspectrum = plt.figure(figsize=(9, 8))
    
    ellOUT = np.arange(len(clOut_fullsky))
    plt.plot(ellOUT, ellOUT * (ellOUT+1) * clOut_fullsky/(2*np.pi),color='b',marker='.',linestyle='-',label="c_l fullsky")
    #plt.plot(ellIN, ellIN * (ellIN+1) * clIN/(2*np.pi),color='r',marker='.')#
    
    ellOUT = np.arange(len(clOut_maskGauss))
    plt.plot(ellOUT, ellOUT * (ellOUT+1) * clOut_maskWindow/(2*np.pi),color='pink',marker='.',linestyle='-',linewidth="1.5",label="c_l of mask")#  
    
    ellOUT = np.arange(len(clOut_cutsky))
    plt.plot(ellOUT, ellOUT * (ellOUT+1) * clOut_cutsky/(2*np.pi),color='g',marker='.',linestyle='-',label="c_l masked")
    #plt.plot(ellOUT, ellOUT * (ellOUT+1) * clOut_mask/(2*np.pi),color='y',marker='.',linestyle='-')
    
    ellOUT = np.arange(len(clOut_deconvolved))
    plt.errorbar(clOut_deconvolved[:,0], clOut_deconvolved[:,0] * (clOut_deconvolved[:,0]+1) * clOut_deconvolved[:,1]/(2*np.pi),xerr=binsize/2.,color='c',marker='',linestyle='-',linewidth="1.5",label="c_l unwindowed (binned)")#  
    #plt.plot(outputarray1[:,0], outputarray1[:,0] * (outputarray1[:,0]+1) * outputarray1[:,1]/(2*np.pi),color='k',marker='.',linestyle='-')
     
    #plt.plot(cl_pseudo_cutskyGauss_spice[:,0], cl_pseudo_cutskyGauss_spice[:,0] * (cl_pseudo_cutskyGauss_spice[:,0]+1) * cl_pseudo_cutskyGauss_spice[:,1]/(2*np.pi),color='brown',marker='.',linestyle='',linewidth="1.5")#  
    #plt.plot(cl_pseudo_cutskyWindow_spice[:,0], cl_pseudo_cutskyWindow_spice[:,0] * (cl_pseudo_cutskyWindow_spice[:,0]+1) * cl_pseudo_cutskyWindow_spice[:,1]/(2*np.pi),color='purple',marker='.',linestyle='',linewidth="1.5")#  
    
    
    #plt.plot(cl_of_maskGauss_spice[:,0], cl_of_maskGauss_spice[:,0] * (cl_of_maskGauss_spice[:,0]+1) * cl_of_maskGauss_spice[:,1]/(2*np.pi),color='orange',marker='.',linestyle='-',linewidth="1.5")#  

    #plt.plot(cl_deconvolved_cutskyGauss_spice[:,0], cl_deconvolved_cutskyGauss_spice[:,0] * (cl_deconvolved_cutskyGauss_spice[:,0]+1) * cl_deconvolved_cutskyGauss_spice[:,1]/(2*np.pi),color='orange',marker='.',linestyle='',linewidth="1.5")#  
    #plt.plot(cl_deconvolved_cutskyWindow_spice[:,0], cl_deconvolved_cutskyWindow_spice[:,0] * (cl_deconvolved_cutskyWindow_spice[:,0]+1) * cl_deconvolved_cutskyWindow_spice[:,1]/(2*np.pi),color='pink',marker='.',linestyle='',linewidth="1.5")#  


    plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1,1.1*len(clOut_fullsky)])
    pylab.ylim([10**-12,LMAX * (LMAX+1) * max(clOut_fullsky)/(2*np.pi)])
    print max(clIN)
    plt.legend()
    plt.show()
    
    print " toyMC.checkFolding.py finished."
    print ""


if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
