#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np

# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import time
import os
import sys
import getopt
#from matplotlib.backends.backend_pdf import PdfPages as pdf
#from textwrap import wrap
import shutil # for file copying
import pyfits


# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hn:r:f:p:e:g:d:s:o:",["nside=","roi=","fov=","psf=","events=","fsig=","fDM=","s=","dirout="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print ' -n <NSIDE>          (Standard: 2^9)'
    print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
    print ' -r <ROIwidthfactor> (Standard: 10)'
    print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
    print ' -e <nevents>        (Standard: 10^4)'
    print ' -g <fsig>           (Standard: 0.0)'
    print ' -d <fDM>            (Standard: 0.0)'
    print ' -s <index s>,       (Standard: 0.0)'
    print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print ' -n <NSIDE>          (Standard: 2^9)'
        print ' -f <sigmaFOVdeg>    (Standard: 2.5°)'
        print ' -r <ROIwidthfactor> (Standard: 10)'
        print ' -p <sigmaPSFdeg>    (Standard: 0.1°)'
        print ' -e <nevents>        (Standard: 10^4)'
        print ' -g <fsig>           (Standard: 0.0)'
        print ' -d <fDM>            (Standard: 0.0)'
        print ' -s <index s>,       (Standard: 0.0)'
        print ' -o <output dir.>    (Standard: "/afs/ifh.de/group/cta/scratch/mhuetten/workdata/anisotropyMC/")'
        sys.exit()
    elif opt in ("-n", "--nside"):
        NSIDE = int(arg)
    elif opt in ("-r", "--roi"):
        ROIwidthfactor = float(arg)
    elif opt in ("-f", "--fov"):
        sigmaFOVdeg = float(arg)
    elif opt in ("-p", "--psf"):
        sigmaPSFdeg = float(arg)
    elif opt in ("-e", "--events"):
        nevents = int(arg)
    elif opt in ("-g", "--fsig"):
        fsig = float(arg)
    elif opt in ("-d", "--fDM"):
        fDM = float(arg)
    elif opt in ("-s", "--s"):
        s = float(arg) 
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg


phi0deg = 0.0
theta0deg = 0.0
start_time = time.time()
## first computations from input variables:

# calculate number of pixels on sphere from NSIDE:
npix=hp.nside2npix(NSIDE)
# give celestial position in rads:
phi0 =  np.pi*phi0deg/180.0
phi0lat = -phi0 # healpy counts phi clockwise!!
theta0 = np.pi*theta0deg/180.0
# give field of view in radians:
sigmaFOV = 0.5*np.pi*sigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# ROI width:
ROIwidth = ROIwidthfactor*2*sigmaFOV
ROIwidthdeg = np.pi*ROIwidth/180
# give PSF in radians:
sigmaPSF = 0.5*np.pi*sigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
# give resolution of skymap in rads, determined by grid resolution:
resolution = hp.nside2resol(NSIDE)
# give resolution of skymap in degs, determined by grid resolution:
resolutiondeg = 180/np.pi*hp.nside2resol(NSIDE)
lmax = int(np.pi/hp.nside2resol(NSIDE))

# Constrain calculation to ROI:
ROIproportion = 0.5*(1.0-np.cos(ROIwidth/2.0))
npixROI = int(ROIproportion * npix)
print " Number of grid pixels in ROI:", npixROI



FOVmask = np.zeros(npixROI) # gaussian weighted mask of field of view
eventmap = np.zeros(npix) # simulated event skymap  containing background and signal events

print " "
print " Generate gaussian mask of field of view..."
for ROIpixelnr in range(npixROI):
    #pixelnr = ROI[ROIpixelnr] # 
    angles = hp.pix2ang(NSIDE, ROIpixelnr,nest=False) #pixelnr
    thetaFOV  = angles[0]
    phiFOV = -angles[1]
    # here, the shape of the acceptance is defined:
    FOVmask[ROIpixelnr] = np.e**(-0.5*(np.arccos(np.sin(theta0-np.pi/2)*np.sin(thetaFOV-np.pi/2) + np.cos(theta0-np.pi/2)*np.cos(thetaFOV-np.pi/2)*np.cos(phi0-phiFOV)))**2/(sigmaFOV)**2)
print " FOVmask filled after", round((time.time() - start_time)/60,2), "minutes." 


print " Load skymap.fits:"
filename = str(s)+"_skymap"
inputname_skymap = str(outputdirectory)+"/%s.fits" % filename 
skymap = hp.read_map(inputname_skymap)


print " "
print " Now start Monte Carlo: Fill eventmap with events..."
# now repeat above procedure until n events are counted:
# count computation time

skymap = (skymap-min(skymap))/(max(skymap)-min(skymap))
hp.write_map(inputname_skymap, skymap)

eventnumber = 0 # to count events from zero to nevents
backgroundcounts = 0 # to later check ratio between signal and background events.
signalcounts = 0  

skymapRED = np.zeros(npixROI) # skymap reduced to ROI
for ROIpixelnr in range(npixROI):
    skymapRED[ROIpixelnr] = skymap[ROIpixelnr] #skymap[ROI[ROIpixelnr]]
    
del skymap

while eventnumber<nevents:
    eventbool = 0
    # first choose if signal or background event:
    randomnumber1 = rn.randint(0, 10*nevents)/(10.0*nevents)
    if randomnumber1>fsig:
            # it is a background event, now select arbitrary pixel within the ROI:
            randomROIpixelnr = rn.randint(0, npixROI-1)
            #randompixelnr = ROI[randomROIpixelnr] # will be needed to fill eventmap
            # the event has not yet detected, still has to pass acceptance function of fov:
            randomnumber3 = rn.randint(0, 10*nevents)/(10.0*nevents)
            if randomnumber3<FOVmask[randomROIpixelnr]:
                # o.k., has succesfully passed the acceptance window, now it really is detected.
                backgroundcounts = backgroundcounts + 1
                eventbool = 1                 
                #info = " background!"
    else:
            # It's a signal event! Now, don't select pixel uniformly distributed within the ROI,
            # but according to the probability skymap:
            signalbool = 0
            while signalbool==0:
                randomROIpixelnr = rn.randint(0, npixROI-1)
                #randompixelnr = ROI[randomROIpixelnr] # willl be needed to fill eventmap
                randomnumber4 = rn.randint(0, 10*nevents)/(10.0*nevents)
                if randomnumber4<skymapRED[randomROIpixelnr]:
                    signalbool = 1
                    # great, at point [pixelnr] really a signal event takes place, but again has to 
                    # pass the acceptance function:
                    randomnumber3 = rn.randint(0, 10*nevents)/(10.0*nevents)
                    if randomnumber3<FOVmask[randomROIpixelnr]:
                        # o.k., has succesfully passed the acceptance window, now it really is detected!
                        signalcounts = signalcounts + 1
                        eventbool = 1
                        #info = " signal!"
    # now finally count event:
    if eventbool==1:
            ## count event at pixel "randompixelnr", but still displace by PSF:
            # get angular position of event:
            angles = hp.pix2ang(NSIDE, randomROIpixelnr,nest=False) #randomROIpixelnr
            thetaEVENT  = angles[0]
            phiEVENT = -angles[1]
            #get radial coordinate of displacement by a gaussian distribution:
            gauss1 = rn.gauss(0, sigmaPSF)
            gauss2 = rn.gauss(0, sigmaPSF)
            #get angular distance of displacement:
            d = np.sqrt(gauss1**2 + gauss2**2)
            # correct sign lost by arcustangens:
            if gauss1<0: 
                philocal =  np.arctan(gauss2/gauss1)+np.pi
            else:
                philocal =  np.arctan(gauss2/gauss1)
            # now apply rotation matrix to displace event by distance d, angle philocal not from the north pole, but from theta_event, phi_event
            x  = np.sin(phiEVENT) * np.sin(d) * np.cos(philocal) - np.cos(thetaEVENT) * np.cos(phiEVENT) * np.sin(d) * np.sin(philocal) + np.sin(thetaEVENT) * np.cos(phiEVENT) * np.cos(d) 
            y  = np.cos(phiEVENT) * np.sin(d) * np.cos(philocal) + np.cos(thetaEVENT) * np.sin(phiEVENT) * np.sin(d) * np.sin(philocal) - np.sin(thetaEVENT) * np.sin(phiEVENT) * np.cos(d)
            z  =                                                   np.sin(thetaEVENT) *                    np.sin(d) * np.sin(philocal) + np.cos(thetaEVENT) *                    np.cos(d) 
            randompixelnr = hp.vec2pix(NSIDE,x ,y ,z , nest=False)
         
            eventmap[randompixelnr] = eventmap[randompixelnr] + 1
            eventnumber = eventnumber + 1
            # print out computation times:
            if eventnumber in [0.1*nevents, 0.2*nevents, 0.3*nevents, 0.4*nevents, 0.5*nevents, 0.6*nevents, 0.7*nevents, 0.8*nevents, 0.9*nevents]:
                print int(eventnumber*100/nevents), "% done after", round((time.time() - start_time)/60,2), "minutes."



del FOVmask             

print " Eventmap filled with total", nevents, "events." 
print " Signal events: ", signalcounts
print " Background events: ", backgroundcounts
print " "
# smoothe eventmap with PSF:
#print "Smoothing eventmap within PSF..."
#eventmap = hp.smoothing(eventmap, fwhm=sigmaPSF)



#print "Saving plots of eventmap..."
# plot event map:
#plot_eventmap = plt.figure(figsize=(9,6.1))
#plt.axis('off')
#title = 'Event skymap for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s$^\circ$, PSF = %s$^\circ$, DM ratio = %s, s = %s, resolution = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,s,round(resolutiondeg,2))
#plt.title('\n'.join(wrap(title,90)))
##plt.subplots_adjust(top=0.95)  
#hp.mollview(eventmap,nest=False, fig=2, unit='events per pixel', title='') 
#hp.graticule(dpar=5, dmer=5)
#outputfile_plots.savefig(plot_eventmap)

# # plot 2d/profile:
# phimax = 5 * sigmaFOV # in deg
# phipixelnr = 2*int(phimax/resolution)+1 # always an odd number
# deltaphi = 2*phimax/(phipixelnr-1) # in deg
# phi = np.zeros(phipixelnr)
# profile = np.zeros(phipixelnr)
# pixelnr = np.zeros(phipixelnr)
# gausscurve = np.zeros(phipixelnr)
# for i in range(phipixelnr):
#     phi[i] = -phimax + i * deltaphi
#     pixelnr[i] = hp.ang2pix(NSIDE, theta0, phi0lat +phi[i],nest=False)
#     profile[i] = eventmap[pixelnr[i]]
# for i in range((phipixelnr-1)/2):
#     phi[i] = i * deltaphi
#     gausscurve[(phipixelnr-1)/2-i] = max(profile)*np.exp(-0.5*phi[i]**2/(sigmaFOV )**2) 
#     gausscurve[i+(phipixelnr-1)/2] = max(profile)*np.exp(-0.5*phi[i]**2/(sigmaFOV )**2)
#     phi[i] = -phimax + i * deltaphi
# deltaphideg = deltaphi*180/np.pi
# phideg = 180*phi/np.pi      
# plot_eventmapprofile = plt.figure(figsize=(9, 7))
# plt.bar(phideg, profile , width=deltaphideg,  facecolor='g')    
# plt.plot(phideg,gausscurve,'b',linewidth=1.5)
# plt.xlabel(' Delta phi in degrees (phi =  phi_0 +- Delta phi)')
# plt.ylabel(' event counts per pixel')
# title ='Profile of  event skymap at  theta theta_0  for total events n = %s, signal-to-noise ratio = %s, Gaussian FOV = %s$^\circ$, PSF = %s$^\circ$, DM ratio = %s, s = %s, resolution = %s$^\circ$, # of slots: %s, delta phi = %s$^\circ$'%(nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,s,round(resolutiondeg,2),phipixelnr,round(deltaphideg,2))
# plt.title('\n'.join(wrap(title,90)))
# plt.subplots_adjust(top=0.87) 
# outputfile_plots.savefig(plot_eventmapprofile)
# plt.close()
  
#### evaluate power spectrum:
print ""

# normalize eventmap:
eventmap = eventmap*npix/nevents
 
print " Saving eventmap to file..."

# Create Skymap Filename:
filename = str(s)+"_eventmap"
outputname_eventmap = str(outputdirectory)+"/%s.fits" % filename 

hp.write_map(outputname_eventmap, eventmap)

 
# compute power spectrum:


# Create Output Spectrum Filename:
filename = str(s)+"_clOutput"
outputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 


print " Write Anafast parameter file anafast.par..."
# Create Anafast Input parameter file:
filename = str(s)+"_anafast"
outputname_anafastpar = str(outputdirectory)+"/%s.par" % filename 
outputfile_anafastpar = open(outputname_anafastpar, 'wb')


# Fill Anafast parameter file Anafast.par:
outputfile_anafastpar.write("simul_type = 1\n\n")
outputfile_anafastpar.write("nlmax = ")
outputfile_anafastpar.write(str(lmax))
outputfile_anafastpar.write("\n\n")
outputfile_anafastpar.write("infile = ")
outputfile_anafastpar.write(outputname_eventmap)
outputfile_anafastpar.write("\n\n") 
outputfile_anafastpar.write("maskfile = ''\n\n")
outputfile_anafastpar.write("theta_cut_deg = 0.00\n\n")
outputfile_anafastpar.write("regression = 2\n\n")
outputfile_anafastpar.write("plmfile = ''\n\n")
outputfile_anafastpar.write("outfile = ")
outputfile_anafastpar.write(outputname_clOutput)
outputfile_anafastpar.write("\n\n")
outputfile_anafastpar.write("outfile_alms = ''\n\n") #outfile_alms=$ODIR/${SPARAMETER}_alm.fits 
outputfile_anafastpar.write("won = 0\n\n")
outputfile_anafastpar.write("iter_order = 0")
outputfile_anafastpar.close()

del eventmap

print " Script toyMC.montecarlo.py successfully finished."
#clOUT = hp.anafast(eventmap, lmax=LMAX)


