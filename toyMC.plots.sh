#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
   echo
   echo "Submission script for ToyMonteCarlo Simulation and a one-dimensional parameter list:"
   echo
   echo "toyMC.sub.sh <constantparameters> <parametervariations>" 
   echo
   echo
   echo "  parameter should contain parameter numbers only"
   echo "  example for parameter list:"
   echo "    0.0"
   echo "    0.2"
   echo "    0.5"
   echo

   exit
fi

#read variables from parameter file:

export NSIDE=$(cat $1 | head -n2 | tail -n1)
export sigmaFOVdeg=$(cat $1 | head -n4 | tail -n1)
export phi0deg=$(cat $1 | head -n6 | tail -n1)
export theta0deg=$(cat $1 | head -n8 | tail -n1)
export ROIwidthfactor=$(cat $1 | head -n10 | tail -n1)
export sigmaPSFdeg=$(cat $1 | head -n12 | tail -n1)
export nevents=$(cat $1 | head -n14 | tail -n1)
export fsig=$(cat $1 | head -n16 | tail -n1)
export s=$(cat $1 | head -n18 | tail -n1)
export fDM=$(cat $1 | head -n20 | tail -n1)
export runnumber=$(cat $1 | head -n22 | tail -n1)
export outdir=$(cat $1 | head -n24 | tail -n1)

PLIST=$2


# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"







###############################################################################################################
# number of parameters:
PARAMETERS=`cat $PLIST`




#
#########################################
# loop over all files in files loop
for AFIL in $PARAMETERS
do
   echo "generating plot $AFIL"
  
python $SCRIPTDIR/toyMC.plots.py -n $NSIDE -f $sigmaFOVdeg -r $ROIwidthfactor -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM -s $AFIL -o $outdir/$runnumber #>> Plot.log
 
#rm $outdir/$runnumber/${AFIL}_eventmap.fits
#rm $outdir/$runnumber/${AFIL}_skymap.fits

done

exit

