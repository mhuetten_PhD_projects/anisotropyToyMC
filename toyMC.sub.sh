#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
   echo
   echo "Submission script for ToyMonteCarlo Simulation and a one-dimensional parameter list:"
   echo
   echo "toyMC.sub.sh <constantparameters> <parametervariations>" 
   echo
   echo
   echo "  parameter should contain parameter numbers only"
   echo "  example for parameter list:"
   echo "    0.0"
   echo "    0.2"
   echo "    0.5"
   echo

   exit
fi

#read variables from parameter file:

export NSIDE=$(cat $1 | head -n2 | tail -n1)
export sigmaFOVdeg=$(cat $1 | head -n4 | tail -n1)
export phi0deg=$(cat $1 | head -n6 | tail -n1)
export theta0deg=$(cat $1 | head -n8 | tail -n1)
export ROIwidthfactor=$(cat $1 | head -n10 | tail -n1)
export sigmaPSFdeg=$(cat $1 | head -n12 | tail -n1)
export nevents=$(cat $1 | head -n14 | tail -n1)
export fsig=$(cat $1 | head -n16 | tail -n1)
export s=$(cat $1 | head -n18 | tail -n1)
export fDM=$(cat $1 | head -n20 | tail -n1)
export runnumber=$(cat $1 | head -n22 | tail -n1)
export outdir=$(cat $1 | head -n24 | tail -n1)

PLIST=$2


# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"





cd $outdir
mkdir -p $runnumber
cd $runnumber
ODIR="$(pwd)"
mkdir -p Inputscripts


cd $SCRIPTDIR
cp toyMC.* $ODIR/Inputscripts/

###############################################################################################################
# number of parameters:
PARAMETERS=`cat $PLIST`

#
#########################################
# output directory for shell scripts
# output directory for error/output from batch system
# in case you submit a lot of scripts: QLOG=/dev/null
DATE=`date +"%y%m%d"`
QLOG=$ODIR/Logfiles

rm $ODIR/Logfiles/*$SPARAMETER*

mkdir -p $QLOG
#
#
# skeleton script
FSCRIPT="toyMC.qsub"
#
#########################################
# loop over all files in files loop
for AFIL in $PARAMETERS
do
   echo "now starting parameter run $AFIL"
   FNAM="$QLOG/toyMC-s-$AFIL"

   sed -e "s|RRRRR|$AFIL|" \
       -e "s|PEEED|$QLOG|" \
       -e "s|OODIR|$ODIR|" \
       -e "s|DDDDD|$DATE|" \
       -e "s|NSSSE|$NSIDE|" \
       -e "s|SSSSS|$sigmaFOVdeg|" \
       -e "s|PPPPP|$phi0deg|" \
       -e "s|TTTTT|$theta0deg|" \
       -e "s|ROOOI|$ROIwidthfactor|" \
       -e "s|PSSSF|$sigmaPSFdeg|" \
       -e "s|NNNNN|$nevents|" \
       -e "s|FFFFF|$fDM|" \
       -e "s|SIIIG|$fsig|" \
       -e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh

   chmod u+x $FNAM.sh
   echo $FNAM.sh

qsub -V -l h_cpu=12:00:00 -l h_rt=12:00:00  -l os=sl6 -l h_vmem=10000M -l tmpdir_size=12G -o $QLOG/ -e $QLOG/ "$FNAM.sh"
done

exit

