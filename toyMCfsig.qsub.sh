#$ -S /bin/tcsh
#
# script to analyse VTS raw files (VBF) with eventdisplay
#
# Author: Gernot Maier
#

#####################################
# parameters set by parent script
set SPARAMETER=RRRRR
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DDDDD
set NSIDE=NSSSE
set sigmaFOVdeg=SSSSS
set phizerodeg=PPPPP
set thetazerodeg=TTTTT
set ROIwidthfactor=ROOOI
set sigmaPSFdeg=PSSSF
set nevents=NNNNN
set fDM=FFFFF
set fsig=SIIIG


mkdir -p $ODIR
#mkdir -p $LDIR
echo
echo " **************************************"
echo " This is fancy ToyMonteCarlo Simulation"
echo " **************************************"
echo 
echo " *** INPUT PARAMETERS: ***"
echo 
echo -n " NSIDE: " $NSIDE"\n"
#echo -n "Number of grid pixels on whole sphere: " npix
echo -n " Using gaussian FOV acceptance...\n"
echo -n " Full diameter of FOV acceptance (1 sigma): " $sigmaFOVdeg "degrees\n"
echo -n " Full diameter of gaussian PSF (1 sigma): " $sigmaPSFdeg "degrees\n"
#echo -n "Resolution of skymap in degs: ",resolutiondeg, "°"
echo -n " ROI width factor (* FOV = ROI width): " $ROIwidthfactor "\n"
echo -n " Number of MC events: " $nevents "\n"
echo -n " Ratio fsig: " $fsig "\n"
echo -n " Ratio fDM: " $fDM "\n"
echo -n " Power spectrum index s: " $SPARAMETER "\n"
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE toyMC.sub.sh ***"
echo 

#####################################
# set the right observatory (environmental variables)
#source $EVNDISPSYS/setObservatory.tcsh VERITAS
source /afs/ifh.de/user/m/mhuetten/setbatchenvironment.tcsh VERITAS
echo



rm $ODIR/${fsig}_skymap.fits
rm $ODIR/${fsig}_clInput.fits
rm $ODIR/${fsig}_clOutput.fits

echo " *** Prepare Skymap: ***"
$SCRIPTDIR/toyMC.prepareSkymap.py -n $NSIDE -f $sigmaFOVdeg -r $ROIwidthfactor -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM -s $SPARAMETER -o $ODIR | tee  $ODIR/Logfiles/logfile.txt
echo

echo " *** Generate Skymap with Synfast routine: ***"
cd $HEALPIX #/afs/ifh.de/group/cta/scratch/mhuetten/Programs/healpix
$HEALPIX/binfortran/synfast -s $ODIR/${fsig}_synfast.par | tee -a $ODIR/Logfiles/logfile.txt
cd $SCRIPTDIR
echo

echo " *** Create eventmap: ***"
$SCRIPTDIR/toyMC.montecarlo.py -n $NSIDE -f $sigmaFOVdeg -r $ROIwidthfactor -p $sigmaPSFdeg -e $nevents -g $fsig -d $fDM -s $SPARAMETER -o $ODIR | tee -a $ODIR/Logfiles/logfile.txt
echo

echo " *** Evaluate Output spectrum with Anafast routine: ***"
cd $HEALPIX #/afs/ifh.de/group/cta/scratch/mhuetten/Programs/healpix
$HEALPIX/binfortran/anafast -s $ODIR/${fsig}_anafast.par | tee -a $ODIR/Logfiles/logfile.txt
cd $SCRIPTDIR
echo

##sleep 20

##exit
