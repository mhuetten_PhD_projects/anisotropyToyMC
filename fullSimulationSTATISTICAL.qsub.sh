#$ -S /bin/tcsh
#
# script to perform Dark Matter clumpy simulations AND detector response
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

set PARAMETER=RRRRR
set RUNDIR=RUUUN
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DAATE
set psiZeroDeg=AAAAA
set thetaZeroDeg=BBBBB
set psiWidthDeg=CCCCC
set thetaWidthDeg=DDDDD
set alphaIntDeg=EEEEE
set user_rse=FFFFF

# Physics parameters:
set gSIMU_IS_ANNIHIL_OR_DECAY=GGGGG

# Galactic Halo:
set gGAL_TOT_FLAG_PROFILE=HHHHH	
set gGAL_TOT_SHAPE_PARAMS_0=IIIII
set gGAL_TOT_SHAPE_PARAMS_1=JJJJJ
set gGAL_TOT_SHAPE_PARAMS_2=KKKKK
set gGAL_TOT_RSCALE=LLLLL
set gGAL_RHOSOL=MMMMM
set gGAL_RSOL=NNNNN
set gGAL_RVIR=OOOOO

# Clumps distribution:
set gGAL_DPDV_FLAG_PROFILE=PPPPP
set gGAL_DPDV_SHAPE_PARAMS_0=QQQQQ
set gGAL_DPDV_SHAPE_PARAMS_1=SSSSS
set gGAL_DPDV_SHAPE_PARAMS_2=TTTTT
set gGAL_DPDV_RSCALE=UUUUU
set gGAL_DPDM_SLOPE=VVVVV
set gDM_MMIN_SUBS=WWWWW
set gGAL_SUBS_N_INM1M2=XXXXX

# Clumps inner profile:
set gGAL_CLUMPS_FLAG_PROFILE=YYYYY
set gGAL_CLUMPS_SHAPE_PARAMS_0=ZZZZZ
set gGAL_CLUMPS_SHAPE_PARAMS_1=ZAAAA
set gGAL_CLUMPS_SHAPE_PARAMS_2=ZBBBB
set gGAL_CLUMPS_FLAG_CVIRMVIR=ZCCCC

# Additional clumpy parameters:
set gDM_MMAXFRAC_SUBS=ZDDDD
set gDM_RHOSAT=ZEEEE
set gGAL_SUBS_M1=ZFFFF
set gGAL_SUBS_M2=ZGGGG

# Monte Carlo simulation parameters:
set NSIDE=ZHHHH
set sigmaFOVdeg=ZIIII
set sigmaPSFdeg=ZJJJJ
set nevents=ZKKKK
set fsig=ZLLLL
set fDM=ZMMMM
set realisationnumber=ZNNNN

echo
echo " ****************************************************************"
echo " This is a full simulation of CLUMPY skymap and detector response"
echo " ****************************************************************"
echo 
echo " *** INPUT PARAMETERS for CLUMPY simulation: ***"
echo 
echo -n " Psi position of line of sight: " $psiZeroDeg "degrees\n"
echo -n " Theta position of line of sight: " $thetaZeroDeg "degrees\n"
echo -n " Full width of skymap around line of sight in psi direction: " $psiWidthDeg "degrees\n"
echo -n " Full width of skymap around line of sight in theta direction: " $thetaWidthDeg "degrees\n"
echo -n " grid resolution: " $alphaIntDeg "degrees\n"
echo -n " user_rse: " $user_rse "\n"
echo
echo " *** INPUT PARAMETERS for Toy Monte Carlo simulation: ***"
echo
echo -n " NSIDE: " $NSIDE"\n"
#echo -n "Number of grid pixels on whole sphere: " npix
echo -n " Using gaussian FOV acceptance...\n"
echo -n " Full diameter of FOV acceptance (1 sigma): " $sigmaFOVdeg "degrees\n"
echo -n " Full diameter of gaussian PSF (1 sigma): " $sigmaPSFdeg "degrees\n"
#echo -n "Resolution of skymap in degs: ",resolutiondeg, "°"
echo -n " Number of MC events: " $nevents "\n"
echo -n " Ratio fsig: " $fsig "\n"
echo -n " Ratio fDM: " $fDM "\n"
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE fullSimulationSTATISTICAL.sub.sh ***"
echo 

#####################################
# set the right observatory (environmental variables)
#source $EVNDISPSYS/setObservatory.tcsh VERITAS
source /afs/ifh.de/user/m/mhuetten/setbatchenvironment.tcsh VERITAS
echo

echo " *** modify clumpy parameter file: ***"
sed  's/EEEEZ/'$alphaIntDeg'/' $SCRIPTDIR/dmClumps.clumpy_params.txt > $ODIR/Inputfiles/tmp1.txt
sed	 's/GGGGZ/'$gSIMU_IS_ANNIHIL_OR_DECAY'/' $ODIR/Inputfiles/tmp1.txt > $ODIR/Inputfiles/tmp2.txt
sed  's/HHHHZ/'$gGAL_TOT_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp2.txt > $ODIR/Inputfiles/tmp3.txt
sed  's/IIIIZ/'$gGAL_TOT_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp3.txt > $ODIR/Inputfiles/tmp4.txt
sed  's/JJJJZ/'$gGAL_TOT_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp4.txt > $ODIR/Inputfiles/tmp5.txt
sed  's/KKKKZ/'$gGAL_TOT_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp5.txt > $ODIR/Inputfiles/tmp6.txt
sed  's/LLLLZ/'$gGAL_TOT_RSCALE'/' $ODIR/Inputfiles/tmp6.txt > $ODIR/Inputfiles/tmp7.txt
sed  's/MMMMZ/'$gGAL_RHOSOL'/' $ODIR/Inputfiles/tmp7.txt > $ODIR/Inputfiles/tmp8.txt
sed  's/NNNNZ/'$gGAL_RSOL'/' $ODIR/Inputfiles/tmp8.txt > $ODIR/Inputfiles/tmp9.txt
sed  's/OOOOZ/'$gGAL_RVIR'/' $ODIR/Inputfiles/tmp9.txt > $ODIR/Inputfiles/tmp10.txt
sed  's/PPPPZ/'$gGAL_DPDV_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp10.txt > $ODIR/Inputfiles/tmp11.txt
sed  's/QQQQZ/'$gGAL_DPDV_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp11.txt > $ODIR/Inputfiles/tmp12.txt
sed  's/SSSSZ/'$gGAL_DPDV_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp12.txt > $ODIR/Inputfiles/tmp13.txt
sed  's/TTTTZ/'$gGAL_DPDV_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp13.txt > $ODIR/Inputfiles/tmp14.txt
sed  's/UUUUZ/'$gGAL_DPDV_RSCALE'/' $ODIR/Inputfiles/tmp14.txt > $ODIR/Inputfiles/tmp15.txt
sed  's/VVVVZ/'$gGAL_DPDM_SLOPE'/' $ODIR/Inputfiles/tmp15.txt > $ODIR/Inputfiles/tmp16.txt
sed  's/WWWWZ/'$gDM_MMIN_SUBS'/' $ODIR/Inputfiles/tmp16.txt > $ODIR/Inputfiles/tmp17.txt
sed  's/XXXXZ/'$gGAL_SUBS_N_INM1M2'/' $ODIR/Inputfiles/tmp17.txt > $ODIR/Inputfiles/tmp18.txt
sed  's/YYYYZ/'$gGAL_CLUMPS_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp18.txt > $ODIR/Inputfiles/tmp19.txt
sed  's/ZZZZX/'$gGAL_CLUMPS_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp19.txt > $ODIR/Inputfiles/tmp20.txt
sed  's/ZAAAZ/'$gGAL_CLUMPS_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp20.txt > $ODIR/Inputfiles/tmp21.txt
sed  's/ZBBBZ/'$gGAL_CLUMPS_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp21.txt > $ODIR/Inputfiles/tmp22.txt
sed  's/ZCCCZ/'$gGAL_CLUMPS_FLAG_CVIRMVIR'/' $ODIR/Inputfiles/tmp22.txt > $ODIR/Inputfiles/tmp23.txt
sed  's/ZDDDZ/'$gDM_MMAXFRAC_SUBS'/' $ODIR/Inputfiles/tmp23.txt > $ODIR/Inputfiles/tmp24.txt
sed  's/ZEEEZ/'$gDM_RHOSAT'/' $ODIR/Inputfiles/tmp24.txt > $ODIR/Inputfiles/tmp25.txt
sed  's/ZFFFZ/'$gGAL_SUBS_M1'/' $ODIR/Inputfiles/tmp25.txt > $ODIR/Inputfiles/tmp26.txt
sed  's/ZGGGZ/'$gGAL_SUBS_M2'/' $ODIR/Inputfiles/tmp26.txt > $ODIR/Inputfiles/dmClumps.clumpy_params.txt

rm $ODIR/Inputfiles/tmp*

chmod u+x $ODIR/Inputfiles/dmClumps.clumpy_params.txt

# in case of multiple runs with runparameter: RUNDIR: parent directory, ODIR: directory of specific runparameter
cd $ODIR/

# make directory for all output spectra files:
mkdir -p spectra-clumpy
mkdir -p spectra-mc

# make temporary directory for each realisation:
mkdir -p realisation-${realisationnumber}
cd realisation-${realisationnumber}
   rm skymap.fits
   rm clInput.fits
   rm clOutput.fits

# copy clumpy binary into realisation directory:
cp $CLUMPY/bin/clumpy $ODIR/realisation-${realisationnumber}/

echo " *** start clumpy routine: ***"
$ODIR/realisation-${realisationnumber}/clumpy -gp7 $ODIR/Inputfiles/dmClumps.clumpy_params.txt $psiZeroDeg $thetaZeroDeg $psiWidthDeg $thetaWidthDeg $user_rse | tee $ODIR/Logfiles/realisation-${realisationnumber}_clumpy.log

rm *.drawn

echo
echo " *** transform clumpy output file into healpix format and prepare power spectrum calculation: ***"

python $SCRIPTDIR/dmClumps.skymap.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $ODIR/realisation-${realisationnumber}  -j Jtot  -z fits -m $gDM_MMIN_SUBS


echo " *** evaluate clumpy output spectrum with anafast routine: ***"
cd $HEALPIX #/afs/ifh.de/group/cta/scratch/mhuetten/Programs/healpix
$HEALPIX/binfortran/anafast -s $ODIR/realisation-${realisationnumber}/anafast.par
cd $ODIR/realisation-${realisationnumber}
rm -r anafast.par

# move output spectrum file into parent folder:
mv *.spectrum.fits $ODIR/spectra-clumpy/clumpy-realisation-${realisationnumber}.spectrum.fits

echo
echo " *** now start Monte Carlo simuluation: ***"

echo " *** create eventmap: ***"
mv *.healpix skymap.healpix
$SCRIPTDIR/clumpyMC.montecarlo.py -n $NSIDE -f $sigmaFOVdeg  -p $sigmaPSFdeg -l $psiZeroDeg -t $thetaZeroDeg  -e $nevents -g $fsig -d $fDM -i $ODIR/realisation-${realisationnumber}/skymap.healpix -o $ODIR/realisation-${realisationnumber}
echo

echo " *** Evaluate output spectrum with anafast routine: ***"
cd $HEALPIX #/afs/ifh.de/group/cta/scratch/mhuetten/Programs/healpix
$HEALPIX/binfortran/anafast -s $ODIR/realisation-${realisationnumber}/anafast.par
cd $ODIR/realisation-${realisationnumber}
 
# move output spectrum file into parent folder:
mv clOutput.fits $ODIR/spectra-mc/mc-realisation-${realisationnumber}.spectrum.fits


echo
echo " *** clumpyMC successfully finished. ***"
echo

# remove temporary directory for each realisation with remaining content
cd $ODIR/
rm -r $ODIR/realisation-${realisationnumber}

##sleep 20
echo
echo " *** Job successfully finished ***"
##exit
